set(pathToProject E:/Fakultet/Master/Code/MotionDetection)
set(pathToOpenCv E:/Misc/OpenCV-android-sdk)

set (SRC_RS_PATH src/main/rs)
set(RENDER_SCRIPT_HEADERS_PATH ${ANDROID_NDK}/toolchains/renderscript/prebuilt/${ANDROID_HOST_TAG}/platform/rs)

set (SRC_RS_GENERATED_PATH build/generated/renderscript_source_output_dir/arm7Debug/compileArm7DebugRenderscript/out)

# Sets the minimum version of CMake required to build your native library.
# This ensures that a certain set of CMake features is available to
# your build.

cmake_minimum_required(VERSION 3.6.0)

set(CMAKE_VERBOSE_MAKEFILE on)
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS} -std=gnu++11")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11")
# set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")

include_directories(
        E:/Misc/OpenCV-android-sdk/sdk/native/jni/include
        ${RENDER_SCRIPT_HEADERS_PATH}/cpp
        ${RENDER_SCRIPT_HEADERS_PATH}/scriptc
        ${RENDER_SCRIPT_HEADERS_PATH}
        ${SRC_RS_GENERATED_PATH}
)

link_directories(${ANDROID_NDK}/toolchains/renderscript/prebuilt/${ANDROID_HOST_TAG}/platform/${ANDROID_SYSROOT_ABI})

# Specifies a library name, specifies whether the library is STATIC or
# SHARED, and provides relative paths to the source code. You can
# define multiple libraries by adding multiple add_library() commands,
# and CMake builds them for you. When you build your app, Gradle
# automatically packages shared libraries with your APK.

#--------------------------------RSWrapper--------------------------------------------------------
add_library (RSWrapper STATIC
        src/main/cpp/rswrapper.cpp
        ${SRC_RS_PATH}/backgroundmodel.rs ${SRC_RS_GENERATED_PATH}/ScriptC_backgroundmodel.cpp
        ${SRC_RS_PATH}/preprocess.rs ${SRC_RS_GENERATED_PATH}/ScriptC_preprocess.cpp
        )
set_target_properties(RSWrapper PROPERTIES LINKER_LANGUAGE CXX)
target_compile_options(RSWrapper PRIVATE -std=c++11 -stdlib=libc++ -fno-rtti -fexceptions -Ofast)
target_link_libraries(RSWrapper RScpp_static dl ${log-lib})

#--------------------------------native-lib--------------------------------------------------------
add_library( # Specifies the name of the library.
        native-lib

        # Sets the library as a shared library.
        SHARED

        # Provides a relative path to your source file(s).
        src/main/cpp/native-lib.cpp

        src/main/cpp/backgroundmodel.hpp

        src/main/cpp/featuretracker.hpp

        src/main/cpp/motiondetector.hpp

        src/main/cpp/main.hpp

        src/main/cpp/parameters.hpp

        src/main/cpp/backgroundmodel.cpp

        src/main/cpp/featuretracker.cpp

        src/main/cpp/motiondetector.cpp

        src/main/cpp/main.cpp
        )

set_target_properties(native-lib PROPERTIES LINKER_LANGUAGE CXX)

add_library( lib_opencv SHARED IMPORTED )
set_target_properties(lib_opencv PROPERTIES IMPORTED_LOCATION ${pathToProject}/app/src/main/jniLibs/${ANDROID_ABI}/libopencv_java3.so)
find_library( log-lib log )
target_link_libraries( native-lib RSWrapper $\{log-lib} lib_opencv android log)