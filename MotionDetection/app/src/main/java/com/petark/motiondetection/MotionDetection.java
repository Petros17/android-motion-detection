/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.petark.motiondetection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;

import java.io.File;

import static org.opencv.videoio.Videoio.CV_CAP_PROP_FPS;


public class MotionDetection extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    JavaCameraView javacameraView;
    ImageView resultImageView;
    TextView fpsTextView;
    Mat mRgba;
    Mat videoFrameInput;
    VideoCapture videoCapture;

    boolean isTest = true;
    String[] testFiles = new String[] {"MotionDetection/woman.avi", "MotionDetection/cheetah.avi",
            "MotionDetection/Traffic360p.avi", "MotionDetection/Traffic720p.avi",
            "MotionDetection/PanTiltZoom360p.avi", "MotionDetection/PanTiltZoom720p.avi", "MotionDetection/PanTiltZoom1080p.avi",
            "MotionDetection/balkon640x480.avi", "MotionDetection/balkon640x480_2.avi", "MotionDetection/balkon1080x720.avi"};

    String testFile = testFiles[1];

    BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch(status){
                case BaseLoaderCallback.SUCCESS:{
                    javacameraView.enableView();
                    break;
                }
                default:{
                    super.onManagerConnected(status);
                    break;
                }
            }
        }
    };

    static {
        System.loadLibrary("native-lib");
        System.loadLibrary("opencv_java3");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motion_detection);

        if (!OpenCVLoader.initDebug()) {
            Log.d("OPENCVLOADER", "OPENCV LOADER INITDEBUG NOT WORKING");
        } else {
            Log.d("OPENCVLOADER", "All is well.");
        }

        this.fpsTextView = findViewById(R.id.fpsTextView);
        this.javacameraView = findViewById(R.id.javaCameraView);
        this.resultImageView = findViewById(R.id.resultImageView);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        deleteCache(getApplicationContext());
        javacameraView.setMaxFrameSize(720, 960);
        initCacheDir(getCacheDir().getAbsolutePath());

        if (isTest){
            File sdcard = Environment.getExternalStorageDirectory();

            //Get the text file
            File file = new File(sdcard,testFile);

            if (file.exists()) {
                videoFrameInput = new Mat();
                videoCapture = new VideoCapture();
                videoCapture.open(file.getAbsolutePath());

                if (!videoCapture.isOpened()){
                    Log.d("DebugTag", "Failed to open video capture: " + file.getAbsolutePath());
                    isTest = false;
                }
                else {
                    videoCapture.read(videoFrameInput);
                    javacameraView.setVisibility(View.INVISIBLE);
                }
            }
            else {
                Log.d("DebugTag", "Failed to locate video file: " + file.getAbsolutePath());
                isTest = false;
            }
        }

        if (!isTest){
            javacameraView.setVisibility(SurfaceView.VISIBLE);
            javacameraView.setCvCameraViewListener(this);
        } else {
            resultImageView.setVisibility(SurfaceView.VISIBLE);
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_background);
            resultImageView.setImageBitmap(icon);

            new Thread() {
                @Override
                public void run() {
                    runTest();
                }
            }.start();
        }
    }

    private void runTest(){
        double videoFps = videoCapture.get(CV_CAP_PROP_FPS);
        if (videoFps <= 0) videoFps = 24;

        long millisForFps = 1000 / (int)videoFps;

        long totalFps = 0;
        long totalFrames = 0;
        do{
            long millis = System.currentTimeMillis();
            Mat converted = ConvertToColorSpace(videoFrameInput);

            int fps = imgProcess(converted.getNativeObjAddr());
            totalFps+=fps;
            totalFrames++;

            Mat rgb = new Mat();
            Imgproc.cvtColor(converted, rgb, Imgproc.COLOR_RGB2BGRA);
            Bitmap bmp = Bitmap.createBitmap(rgb.cols(), rgb.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(rgb, bmp);
            long time = System.currentTimeMillis() - millis;

            if (time < millisForFps){
                try{
                    Thread.sleep(millisForFps - time);
                }
                catch(InterruptedException ex){}
            }

            final Bitmap bmpToSet = bmp;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    resultImageView.setImageBitmap(bmpToSet);
                    resultImageView.invalidate();
                }
            });

            Log.d("DebugTag", "Current average is: " + (totalFps * 1.0 / totalFrames));
        }  while (videoCapture.read(videoFrameInput));
    }

    private Mat ConvertToColorSpace(Mat img){
        Mat converted = new Mat();
        Imgproc.cvtColor(img, converted, Imgproc.COLOR_RGB2RGBA);
        return converted;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (javacameraView != null){
            javacameraView.disableView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(javacameraView!=null){
            javacameraView.disableView();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (OpenCVLoader.initDebug()){
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        } else{
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mLoaderCallback);
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
        mRgba.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        final int fps = imgProcess(mRgba.getNativeObjAddr());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fpsTextView.setText("FPS: " + fps);
            }
        });

        return mRgba;
    }

    public native int imgProcess(long inputMat);
    public native void initCacheDir(String cacheDir);

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) { e.printStackTrace();}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }
}
