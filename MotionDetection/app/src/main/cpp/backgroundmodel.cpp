#include "backgroundmodel.hpp"
#include "featuretracker.hpp"
#include <android/log.h>
#include <memory>

// ---------------------------------------------------------------------------------------
//  BackgroundModel::Init
//
//      Initialize Render script wrapper with proper dimensions.
//      Perform initial motion compensation and model update, to init all internal structs.
//
void BackgroundModel::Init(cv::Mat const &inputImg)
{
    int modelWidth = inputImg.cols / BLOCK_SIZE;
    int modelHeight = inputImg.rows / BLOCK_SIZE;

    m_rsWrapper->SetModelDimensions(modelWidth, modelHeight);

    // Update with homography I
    //
    double h[9];
    h[0] = 1.0; h[1] = 0.0; h[2] = 0.0;
    h[3] = 0.0; h[4] = 1.0; h[5] = 0.0;
    h[6] = 0.0; h[7] = 0.0; h[8] = 1.0;

    MotionCompensate(h, inputImg);
    Update();
}

// ---------------------------------------------------------------------------------------
//  BackgroundModel::InitRenderScript
//
//      Initialize render script object with cache dir.
//      Run test script, just to verify versioning.
//      TODO: remove this Test() once sure it's not needed.
//
void BackgroundModel::InitRenderScript(const char *appCacheDir)
{
    m_rsWrapper = std::unique_ptr<RSWrapper>(new RSWrapper(appCacheDir));
    m_rsWrapper->Test();
}

// ---------------------------------------------------------------------------------------
//  BackgroundModel::CopyInputToRS
//
//      Copy input image data to renderscript memory.
//      This will be invoked before motion compensation, so that by the time Update of the
//      model is invoked, input is in Script memory, so that moving pixels can be marked.
//
void BackgroundModel::CopyInputToRS(const cv::Mat& inputImg)
{
    m_rsWrapper->CopyInput(inputImg.data);
}

// ---------------------------------------------------------------------------------------
//  BackgroundModel::DisplayResult
//
//      Copy results back from script memory, so that they can be displayed.
//
void BackgroundModel::DisplayResult(cv::Mat& inputImg)
{
    m_rsWrapper->DisplayResult(inputImg.data);
}

// ---------------------------------------------------------------------------------------
//  BackgroundModel::MotionCompensate
//
//      Compensate for the motion of the camera, so that it does not impact actual motion
//      detection.
//
void BackgroundModel::MotionCompensate(const double hInput[9], cv::Mat const &curImg)
{
    // Need conversion to float, since GPU cannot be engaged (or even compiled) with doubles.
    // No relevant precision loss will happen due to this conversion.
    // Reason it wasn't float to begin with is that OpenCV does homography calculation
    // on doubles.
    //
    float h[9];
    for (int i = 0; i < 9; i++) h[i] = static_cast<float>(hInput[i]);

    m_rsWrapper->MotionCompensate(curImg.data, h);
}

// ---------------------------------------------------------------------------------------
//  BackgroundModel::Update
//
//      Updates the model with latest data, and marks pixels as foreground on the input.
//
void BackgroundModel::Update()
{
    m_rsWrapper->Update();
}