//
// Created by Petar on 04.05.2019..
//

#include "rswrapper.hpp"
#include <android/log.h>
#include <string>
#include "parameters.hpp"

using namespace android::RSC;

// ---------------------------------------------------------------------------------------
//  RSWrapper::RSWrapper
//
//      Construct wrapper, and initialize RenderScript objects with cache dir of the app.
//
RSWrapper::RSWrapper(const char *appCacheDir)
{
    this->m_RS = new RS();
    m_RS->init(appCacheDir);

    m_scriptC_backgroundmodel = std::unique_ptr<ScriptC_backgroundmodel>(new ScriptC_backgroundmodel(m_RS));
    m_scriptC_preprocess = std::unique_ptr<ScriptC_preprocess>(new ScriptC_preprocess(m_RS));

    m_homographyMatrixAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), 9);

    m_scriptC_backgroundmodel->bind_homographyMatrix(m_homographyMatrixAlloc);
}

//---------------------------------------------------
// RSWrapper::SetModelDimensions
//
//      Initializes model width and height inside the script.
//
void RSWrapper::SetModelDimensions(int modelWidth, int modelHeight){
    modelSize = modelHeight * modelWidth;

    m_scriptC_backgroundmodel->set_modelWidth(modelWidth);
    m_scriptC_backgroundmodel->set_modelHeight(modelHeight);
    m_scriptC_backgroundmodel->set_blockCount(modelSize);
    m_scriptC_backgroundmodel->set_inputImageCols(modelWidth * BLOCK_SIZE);
    m_scriptC_backgroundmodel->set_inputImageRows(modelHeight * BLOCK_SIZE);

    __android_log_print(ANDROID_LOG_INFO, "DebugTag", "model width: %d, model height %d", modelWidth, modelHeight);
    BindAllocations();

    m_pInputForGreyScale = Allocation::createSized(m_RS, Element::U8_4(m_RS), modelSize * BLOCK_SIZE * BLOCK_SIZE, RS_ALLOCATION_USAGE_SCRIPT);
}

// ---------------------------------------------------------------------------------------
//  RSWrapper::MotionCompensate
//
//      Motion compensate for the movement of the camera.
//
void RSWrapper::MotionCompensate(unsigned char *pCurData, const float h[9]) {
    // Copy current frame and homography data to RS.
    //
    m_pCurGrayImageAlloc->copy1DFrom(pCurData);
    m_homographyMatrixAlloc->copy1DFrom(h);

    // Calculate offsets and indices, based on the homography.
    //
    m_scriptC_backgroundmodel->forEach_calculatePositionOffset(m_idxNow);

    // Synchronize memory, to make sure it's safe to access and previous kernel
    // completed.
    //
    m_diAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_djAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_idxNewIAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_idxNewJAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_MeanAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_VarAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_AgeAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);

    // Run motion compensation on each block.
    //
    m_scriptC_backgroundmodel->forEach_motionCompensate(m_idxNow);
}

// ---------------------------------------------------------------------------------------
//  RSWrapper::Update
//
//      Update the model and mark foreground pixels.
//
void RSWrapper::Update()
{
    UpdateTempParamModel();
    UpdateMeanVarAgeOutput();
}

// ---------------------------------------------------------------------------------------
//  RSWrapper::UpdateTempParamModel
//
//      After motion of the camera is compensated, update temp parameters (mean, age, var)
//      with findings.
//
void RSWrapper::UpdateTempParamModel() {
    m_tempMeanAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_tempAgeAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_tempVarAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);

    m_scriptC_backgroundmodel->forEach_updateTempParamModel(m_idxNow);
}

// ---------------------------------------------------------------------------------------
//  RSWrapper::UpdateMeanVarAgeOutput
//
//      After motion of the camera is compensated, and temp model params are updated,
//      use that data to mark actual foreground pixels.
//
void RSWrapper::UpdateMeanVarAgeOutput() {
    m_tempMeanAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_tempAgeAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_tempVarAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_idxNow->syncAll(RS_ALLOCATION_USAGE_SCRIPT);

    m_scriptC_backgroundmodel->forEach_updateMeanVarAgeOutput(m_idxNow);
}

// ---------------------------------------------------------------------------------------
//  RSWrapper::CopyInput
//
//      Copy input image to the RenderScript allocation. This input will be used to mark
//      actual foreground pixels.
//
void RSWrapper::CopyInput(unsigned char* inputImgData){
    m_pInputImage->copy1DFrom(inputImgData);
}

// ---------------------------------------------------------------------------------------
//  RSWrapper::DisplayResult
//
//      Copy input image back from RenderScript. Foreground is marked on the input itself,
//      so copy it back so it can be displayed.
//
void RSWrapper::DisplayResult(unsigned char *destinationData) {
    m_pInputImage->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_pInputImage->copy1DTo(destinationData);
}

// ---------------------------------------------------------------------------------------
//  RSWrapper::Test
//
//      Run dummy kernel, which will change internal homography data with test counter.
//      Used to check that latest version of script is running.
//      TODO: remove once confirmed it's not needed.
//
void RSWrapper::Test(){
    float h[9];
    m_scriptC_backgroundmodel->forEach_dummyKernel(m_homographyMatrixAlloc);
    m_homographyMatrixAlloc->copy1DTo(h);
    std::string result = "";
    for (auto hh : h) result += " " + std::to_string(hh);
    __android_log_print(ANDROID_LOG_INFO, "DebugTag", "h: %s.", result.c_str());
}

void RSWrapper::ToGreyscale(unsigned char *pCurData, unsigned char* output) {
    m_pInputForGreyScale->copy1DFrom(pCurData);
    m_scriptC_preprocess->forEach_gray(m_pInputForGreyScale, m_pCurGrayImageAlloc);
    m_pCurGrayImageAlloc->copy1DTo(output);
}

// ---------------------------------------------------------------------------------------
//  RSWrapper::BindAllocations
//
//      Allocate and bind space with RenderScript object, based on model size.
//
void RSWrapper::BindAllocations()
{
    // Bind dummy allocation used to iterate over indexes.
    //
    m_idxNow = Allocation::createSized(m_RS, Element::I32(m_RS), modelSize, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_backgroundmodel->bind_modelIdxAlloc(m_idxNow);

    // Create and bind indices allocation.
    //
    m_diAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize, RS_ALLOCATION_USAGE_SCRIPT);
    m_djAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize, RS_ALLOCATION_USAGE_SCRIPT);
    m_idxNewIAlloc = Allocation::createSized(m_RS, Element::I32(m_RS), modelSize, RS_ALLOCATION_USAGE_SCRIPT);
    m_idxNewJAlloc = Allocation::createSized(m_RS, Element::I32(m_RS), modelSize, RS_ALLOCATION_USAGE_SCRIPT);

    m_scriptC_backgroundmodel->bind_diAlloc(m_diAlloc);
    m_scriptC_backgroundmodel->bind_djAlloc(m_djAlloc);
    m_scriptC_backgroundmodel->bind_idxNewIAlloc(m_idxNewIAlloc);
    m_scriptC_backgroundmodel->bind_idxNewJAlloc(m_idxNewJAlloc);

    // Create and bind parameter allocations (mean, age, var)
    //
    m_MeanAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_tempMeanAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_backgroundmodel->bind_meanAlloc(m_MeanAlloc);
    m_scriptC_backgroundmodel->bind_tempMeanAlloc(m_tempMeanAlloc);

    m_AgeAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_tempAgeAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_backgroundmodel->bind_ageAlloc(m_AgeAlloc);
    m_scriptC_backgroundmodel->bind_tempAgeAlloc(m_tempAgeAlloc);

    m_VarAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_tempVarAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_backgroundmodel->bind_varAlloc(m_VarAlloc);
    m_scriptC_backgroundmodel->bind_tempVarAlloc(m_tempVarAlloc);

    // Create and bind image pixel data info, for current frame in grey and RGBA.
    //
    m_pCurGrayImageAlloc = Allocation::createSized(m_RS, Element::U8(m_RS), modelSize * BLOCK_SIZE * BLOCK_SIZE, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_backgroundmodel->bind_pCur(m_pCurGrayImageAlloc);
    m_pInputImage = Allocation::createSized(m_RS, Element::U8_4(m_RS),  modelSize * BLOCK_SIZE * BLOCK_SIZE, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_backgroundmodel->bind_pInputImg(m_pInputImage);
}
