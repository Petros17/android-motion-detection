#pragma once

// Includes for this wrapper
#include "featuretracker.hpp"
#include "backgroundmodel.hpp"

static int64_t getTimeNsec() {
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);
	return (int64_t) now.tv_sec*1000000000LL + now.tv_nsec;
}

//--------------------------------------------------------------------------
//  Class MotionDetector
//
//      Encapsulates logic behind motion detection.
//      This class is responsible for compensating for motion of the camera,
//      and then detecting motion between two frames, with marking detected
//      pixels appropriately.
//
class MotionDetector {
 private:
    cv::Mat imgTemp;
	cv::Mat imgGray;

    FeatureTracker m_featureTracker;
    BackgroundModel m_backgroundModel;

    RSWrapper* rsWrapper;
 public:
    void Init(const cv::Mat &inImg);
	void Run(cv::Mat &in);
	void InitCacheDir(const char* appCacheDir);
};
