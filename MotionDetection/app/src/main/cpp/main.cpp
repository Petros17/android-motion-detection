#include "main.hpp"
#include "motiondetector.hpp"
#include <android/log.h>
#include <cinttypes>

static const int MAX_FRAME_COUNT = 10000000;
static float totalFps = 0;
static int avg = 30;
static int frame_count = 0;
static MotionDetector *pMotionDetector = new MotionDetector();

void initCacheDir(const char* appCacheDir) {
	pMotionDetector->InitCacheDir(appCacheDir);
}

int doIt(cv::Mat& img)
{
	if (0 == frame_count) {
		// Init the wrapper for first frame
		pMotionDetector->Init(img);
	} else {
	    int64_t startNss = getTimeNsec();
        // Run detection
		pMotionDetector->Run(img);
		int64_t endNss = getTimeNsec();

		int64_t nsDiff = (endNss - startNss);
		float fps = (1000000000.0f/nsDiff);
		totalFps += fps;
#ifdef TEST_ENV
        __android_log_print(ANDROID_LOG_INFO, "DebugTag", "Execution time: %" PRId64 ", FPS: %.3f\n", nsDiff, fps);
#endif
	}

    if (frame_count > MAX_FRAME_COUNT) {
        totalFps /= frame_count;
        frame_count = 1;
    }

    if (frame_count % 30 == 1) {
        avg = totalFps/frame_count;
    }

	frame_count++;

	return avg;
}