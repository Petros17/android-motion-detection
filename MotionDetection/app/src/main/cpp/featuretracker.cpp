#include "featuretracker.hpp"

#include <vector>
#include <string>

#include <opencv2/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/video.hpp>
#include <android/log.h>

cv::TermCriteria termCriteria(cv::TermCriteria::COUNT|cv::TermCriteria::EPS,20,0.03);

// ---------------------------------------------------------------------------------
//  FeatureTracker::Initialize
//
//      Initializes internal structures used for feature tracking, based on initial
//      input image. This method will allocate and size proper vectors.
//
//      Invoked once, on first frame processing. Should always complete successfully.
//
void FeatureTracker::Initialize(cv::Mat const &imgGray)
{
	int ni = imgGray.cols;
	int nj = imgGray.rows;

	// Allocate Maximum possible + some more for safety
	MAX_COUNT = (float (ni) / float (GRID_SIZE_W) + 1.0)*(float (nj) / float (GRID_SIZE_H) + 1.0);

	defaultCount = ni / GRID_SIZE_W * nj / GRID_SIZE_H;

    // Default points never change. They are used to re-init interesting points on each frame.
    // Interesting points are edges of each grid (pixel block).
    //
    for (int i = 0; i < ni / GRID_SIZE_W - 1; i++)
    for (int j = 0; j < nj / GRID_SIZE_H - 1; j++){
        cv::Point2f point(i * GRID_SIZE_W + GRID_SIZE_W / 2, j * GRID_SIZE_H + GRID_SIZE_H / 2);
        m_defaultPoints.emplace_back(point);
    }

	// Init homography.
	// Initial matrix is:
	//      | 1 0 0 |
	//      | 0 1 0 |
	//      | 0 0 1 |
	for (int i = 0; i < 9; i++)
		m_pMatH.get()[i] = i / 3 == i % 3 ? 1 : 0;
}

// ---------------------------------------------------------------------------------
//  FeatureTracker::Reset
//
//      Resets vectors and counters for the features. This will set interesting feature
//      coordinates to edges of each grid.
//
void FeatureTracker::Reset()
{
    m_points[1].clear();
    m_points[1] = m_defaultPoints;
    m_points[1].resize(m_defaultPoints.size());
    count = defaultCount;
}

// ---------------------------------------------------------------------------------
//  FeatureTracker::RunFeatureTracking
//
//      Track interesting points between two consecutive frames. Takes new grayed frame
//      as input, and by using previous frame and interesting points, calculates where
//      those points now are using Lukas-Kanade tracker.
//      Result of this operation is a homography, later used to motion compensate for
//      the movement of the camera.
//
const double* FeatureTracker::RunFeatureTracking(cv::Mat const &imgGray)
{
	int i, k;
	std::unique_ptr<int[]> pMatch(new int[MAX_COUNT]);

    if (m_imgPrevGray.empty()) {
        imgGray.copyTo(m_imgPrevGray);
    }

    if (count > 0){
        std::vector<float> err;

        // Run feature tracking.
        //
        cv::calcOpticalFlowPyrLK(m_imgPrevGray, imgGray, m_points[0], m_points[1], m_status, err,
                                 cv::Size(win_size, win_size), 3, termCriteria);

        // If status flag is set, there's a match for the point from the previous frame.
        // Save that points index.
        //
        for (i = k = 0; i < m_status.size(); i++) {
            if (!m_status[i])
                continue;

            pMatch[k++] = i;
        }
        count = k;
    }

    // If we have matched at least 10 points, homography can be made.
    //
    if (count >= 10) {
        // Make homography matrix with correspondences
        MakeHomography(pMatch.get());
    } else {
        // Re-initialize homography, with 1 on diagonal.
        //
        for (int ii = 0; ii < 9; ++ii) {
            m_pMatH.get()[ii] = ii % 3 == ii / 3 ? 1.0f : 0.0f;
        }
    }
    Reset();
	SwapData(imgGray);

	return m_pMatH.get();
}

// ---------------------------------------------------------------------------------------
//  FeatureTracker::SwapData
//
//      Swaps data, so that current frame now becomes previous frame in the next iteration.
//
void FeatureTracker::SwapData(cv::Mat const &imgGray)
{
	imgGray.copyTo(m_imgPrevGray);
	std::swap(m_points[1], m_points[0]);
}

// ---------------------------------------------------------------------------------------
//  FeatureTracker::MakeHomography
//
//      Make homography, based on previous and current points.
//      As input, array of indexes where a match was made.
//
void FeatureTracker::MakeHomography(const int *pMatch)
{
    std::vector<cv::Point2f> pt1, pt2;

    pt1.resize(count);
    pt2.resize(count);
    for (int i = 0; i < count; i++){
        pt1[i] = m_points[1][pMatch[i]];
        pt2[i] = m_points[0][pMatch[i]];
    }

    cv::Mat pt1Mat(pt1);
    cv::Mat pt2Mat(pt2);

    cv::Mat _hMat = cv::findHomography(pt1Mat, pt2Mat, CV_RANSAC, 1);
    if (!_hMat.empty()) {
        for (int i = 0; i < 9; i++) {
            m_pMatH.get()[i] = _hMat.at<double>(i);
        }
    }
}
