#pragma once

#define BLOCK_SIZE				(4)
#define BLOCK_SIZE_SQR				(16.0)
#define VARIANCE_INTERPOLATE_PARAM	        (1.0f)

#define MAX_BG_AGE				(30.0f)
#define VAR_MIN_NOISE_T			        (50.0f*50.0f)
#define VAR_DEC_RATIO			        (0.001f)
#define MIN_BG_VAR				(5.0f*5.0f)	//15*15
#define INIT_BG_VAR				(20.0f*20.0f)	//15*15

#define NUM_MODELS		        (2)
#define VAR_THRESH_FG_DETERMINE		(4.0f)
#define VAR_THRESH_MODEL_MATCH		(2.0f)

#define TEST_ENV