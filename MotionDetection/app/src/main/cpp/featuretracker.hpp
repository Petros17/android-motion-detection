#pragma once

#include <opencv2\opencv.hpp>

#define GRID_SIZE_W (32)
#define GRID_SIZE_H (24)

typedef unsigned char BYTE;

//--------------------------------------------------------------------------
//  Class FeatureTracker
//
//      Encapsulates feature tracking, using Kanade-Lukas-Tomasi algorithm
//      Purpose is tracking movement of block corners between two frames.
//      Based on that tracked movement, it will create a homography, later
//      used to compensate the motion of the camera.
//
class FeatureTracker {
 private:
	// Previous grayed image, used as input for LK tracker.
	cv::Mat m_imgPrevGray;

    // Vectors used as input/output of LK tracker
    std::vector<cv::Point2f> m_points[2];
    std::vector<cv::Point2f> m_defaultPoints;
	std::vector<uchar> m_status;

    // Variables used as input/output of LK tracker.
	int count = 0;
	int defaultCount;
    int win_size = 10;
    int MAX_COUNT;

    // Homography matrix, 3x3, an output of LK tracker.
    std::unique_ptr<double> m_pMatH = std::unique_ptr<double>(new double[9]);

 private:
	void SwapData(cv::Mat const &imgGray);
	void MakeHomography(const int *pMatch);
	void Reset();

 public:
	void Initialize(cv::Mat const &imgGray);
	const double* RunFeatureTracking(cv::Mat const &imgGray);
};
