#pragma once
#include	<opencv2/features2d/features2d.hpp>

void initCacheDir(const char* appCacheDir);
int doIt(cv::Mat& img);