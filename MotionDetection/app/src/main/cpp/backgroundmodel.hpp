#pragma once

#include <algorithm>
#include <opencv2/imgproc.hpp>
#include "opencv2\opencv.hpp"
#include "parameters.hpp"
#include "featuretracker.hpp"
#include "rswrapper.hpp"

//--------------------------------------------------------------------------
//  Class BackgroundModel
//
//      Encapsulates data used to differentiate foreground from background.
//      Calculations are done via RenderScript framework.
//
class BackgroundModel {
 private:
	std::unique_ptr<RSWrapper> m_rsWrapper;

 public:
	void Init(cv::Mat const &inputImg);
	void MotionCompensate(const double h[9], cv::Mat const &curImg);
	void Update();
	void InitRenderScript(const char* appCacheDir);

	void CopyInputToRS(const cv::Mat& inputImg);
	void DisplayResult(cv::Mat& inputImg);

	RSWrapper* GetRSWrapper() { return m_rsWrapper.get(); };
};
