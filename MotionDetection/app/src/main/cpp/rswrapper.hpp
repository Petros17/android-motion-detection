#pragma once

#include <RenderScript.h>
#include <ScriptC_backgroundmodel.h>
#include <algorithm>
#include <ScriptC_preprocess.h>

using namespace android::RSC;

//--------------------------------------------------------------------------
//  Class RSWrapper
//
//      Encapsulates interaction with RenderScript framework.
//      Used by BackgroundModel class to perform motion compensation and
//      actual foreground pixel detection.
//
class RSWrapper {
private:
    // RenderScript objects.
    sp<RS> m_RS;
    std::unique_ptr<ScriptC_backgroundmodel> m_scriptC_backgroundmodel;
    std::unique_ptr<ScriptC_preprocess> m_scriptC_preprocess;

    unsigned int modelSize;

    // Allocations linked to RS
    //
    sp<Allocation> m_homographyMatrixAlloc;
    sp<Allocation> m_diAlloc;
    sp<Allocation> m_djAlloc;
    sp<Allocation> m_idxNewIAlloc;
    sp<Allocation> m_idxNewJAlloc;

    // Dummy alloc used to initiate iterating through all blocks.
    //
    sp<Allocation> m_idxNow;

    // Allocations for mean, var, and age.
    //
    sp<Allocation> m_tempMeanAlloc;
    sp<Allocation> m_MeanAlloc;
    sp<Allocation> m_tempAgeAlloc;
    sp<Allocation> m_AgeAlloc;
    sp<Allocation> m_tempVarAlloc;
    sp<Allocation> m_VarAlloc;

    // Allocation for current frame grey image data.
    //
    sp<Allocation> m_pCurGrayImageAlloc;

    // Allocation for current frame, in RGBA format.
    //
    sp<Allocation> m_pInputImage;

    sp<Allocation> m_pInputForGreyScale;

public:
    explicit RSWrapper(const char *appCacheDir);

    void SetModelDimensions(int modelWidth, int modelHeight);

    void MotionCompensate(unsigned char *pCurData, const float h[9]);
    void Update();

    void CopyInput(unsigned char* inputImgData);
    void DisplayResult(unsigned char* destinationData);

    void ToGreyscale(unsigned char* pCurData, unsigned char* output);

    void Test();

private:
    void BindAllocations();

    void UpdateTempParamModel();
    void UpdateMeanVarAgeOutput();
};

