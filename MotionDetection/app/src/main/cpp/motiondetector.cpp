#include <thread>
#include "motiondetector.hpp"
#include "parameters.hpp"
#include <android/log.h>

// ---------------------------------------------------------------------------------------
//  MotionDetector::Init
//
//      Initializes internal structures and classes using initial frame.
//
void MotionDetector::Init(const cv::Mat &img)
{
	// Allocate.
	//
	imgGray = cv::Mat(img.rows, img.cols, IPL_DEPTH_8U, 1);

	cv::cvtColor(img, imgTemp, CV_RGB2GRAY);
	cv::medianBlur(imgTemp, imgGray, 5);

	m_featureTracker.Initialize(imgGray);
    m_backgroundModel.Init(imgGray);

    rsWrapper = m_backgroundModel.GetRSWrapper();
}

// ---------------------------------------------------------------------------------------
//  MotionDetector::Run
//
//      Run motion detection, provided with current frame.
//      Compensate for camera movement, and mark pixels which are foreground.
//      Marking of the pixels is done on the input image itself.
//
void MotionDetector::Run(cv::Mat &img)
{
    auto copyInput = [this, img]() {
        m_backgroundModel.CopyInputToRS(img);
    };

    // Thread used to copy input image to script memory, because the results will
    // be marked on the input itself.
    // Using a separate thread, so that feature tracking and motion compensation
    // can carry on, without waiting for this.
    //
    std::thread copyInputThread(copyInput);

    int64_t startPreprocessNs = getTimeNsec();

	// Pre-process.
	//
	cv::cvtColor(img, imgTemp, CV_RGB2GRAY);
	//rsWrapper->ToGreyscale(img.data, imgTemp.data);
	cv::medianBlur(imgTemp, imgGray, 5);

    int64_t startHomographyGenNs = getTimeNsec();

	// Calculate homography, used to motion compensate for camera movement.
	//
	const double* homographyMatrix = m_featureTracker.RunFeatureTracking(imgGray);

    int64_t startMotionCompensateNs = getTimeNsec();

    m_backgroundModel.MotionCompensate(homographyMatrix, imgGray);

    // Wait for the thread copying the input image to complete.
    // By this point, this is most likely done, but we need to be sure it is
    // before updating the model, since update - as a side effect - displays results.
    //
    copyInputThread.join();

    int64_t startModelUpdateNs = getTimeNsec();

	// Update background model. Part of this method is actually marking the foreground
	// pixels on the input image.
	//
    m_backgroundModel.Update();

    int64_t startResultCopyNs = getTimeNsec();

    // Just copy the results back from script memory.
    //
    m_backgroundModel.DisplayResult(img);

    int64_t endResultCopyNs = getTimeNsec();

#ifdef TEST_ENV
    float preProcessMs = (startHomographyGenNs - startPreprocessNs) / 1000000.0f;
    float homographyGenMs = (startMotionCompensateNs - startHomographyGenNs) / 1000000.0f;
    float motionCompensateMs = (startModelUpdateNs - startMotionCompensateNs) / 1000000.0f;
    float modelUpdateMs = (startResultCopyNs - startModelUpdateNs) / 1000000.0f;
    float resultCopyMs = (endResultCopyNs - startResultCopyNs) / 1000000.0f;

    __android_log_print(ANDROID_LOG_INFO,
            "DebugTag",
            "Preprocess MS: %.3f, homographyGenMs: %.3f, motionCompensateMs: %.3f, modelUpdateMs: %.3f, resultCopyMs: %.3f",
            preProcessMs, homographyGenMs, motionCompensateMs, modelUpdateMs, resultCopyMs);
#endif
}

// ---------------------------------------------------------------------------------------
//  MotionDetector::InitCacheDir
//
//      Initialize cache dir of render script
//
void MotionDetector::InitCacheDir(const char *appCacheDir)
{
	m_backgroundModel.InitRenderScript(appCacheDir);
}