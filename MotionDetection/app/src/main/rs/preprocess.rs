#pragma version(1)
#pragma rs java_package_name(com.petark.motiondetection)
#pragma rs_fp_relaxed

uchar RS_KERNEL gray(uchar4 in) {
    float gr= 0.2125*in.r + 0.7154*in.g + 0.0721*in.b;
    return (uchar)gr;
}