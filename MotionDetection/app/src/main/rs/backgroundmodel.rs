#pragma version(1)
#pragma rs java_package_name(com.petark.motiondetection)
#pragma rs_fp_relaxed

#include "backgroundmodel.rsh"
#include "rs_math.rsh"

static void *memset(void *s, int c, size_t n)
{
   unsigned char* p=s;
   while(n--)
       *p++ = (unsigned char)c;
   return s;
}

int32_t modelWidth;
int32_t modelHeight;
int32_t blockCount;
int32_t inputImageCols;
int32_t inputImageRows;

float* homographyMatrix;
float* diAlloc;
float* djAlloc;
int32_t* idxNewIAlloc;
int32_t* idxNewJAlloc;

float* meanAlloc;
float* tempMeanAlloc;
float* ageAlloc;
float* tempAgeAlloc;
float* varAlloc;
float* tempVarAlloc;

uchar* pCur;
uchar4* pInputImg;

int32_t* modelIdxAlloc;

void RS_KERNEL dummyKernel(float in, uint32_t x) {
    homographyMatrix[x] = x + 1414.0f;
}

void RS_KERNEL calculatePositionOffset(int32_t in, uint32_t x) {
    uint32_t j = x / modelWidth;
    uint32_t i = x - j * modelWidth;

    // x and y coordinates for current model - a bit counter intuitive
    float blockCenterX = BLOCK_SIZE * i + HalfBlockSize;
    float blockCenterY = BLOCK_SIZE * j + HalfBlockSize;

    // transformed coordinates with h
    float newW = homographyMatrix[6] * blockCenterX + homographyMatrix[7] * blockCenterY + homographyMatrix[8];
    float newX = (homographyMatrix[0] * blockCenterX + homographyMatrix[1] * blockCenterY + homographyMatrix[2]) / newW;
    float newY = (homographyMatrix[3] * blockCenterX + homographyMatrix[4] * blockCenterY + homographyMatrix[5]) / newW;

    // transformed i,j coordinates of old position
    float newI = newX / BLOCK_SIZE;
    float newJ = newY / BLOCK_SIZE;

    idxNewIAlloc[x] = (int)floor(newI);
    idxNewJAlloc[x] = (int)floor(newJ);

    diAlloc[x] = newI - (idxNewIAlloc[x] + 0.5f);
    djAlloc[x] = newJ - (idxNewJAlloc[x] + 0.5f);
}

void RS_KERNEL motionCompensate(int32_t in, uint32_t x) {
    float sumW = 0.0f;

    float temp_mean[TOTAL_NEIGHBORS][NUM_MODELS];
    float temp_age[TOTAL_NEIGHBORS][NUM_MODELS];
    float temp_var[TOTAL_NEIGHBORS][NUM_MODELS];
    memset(temp_mean, 0, sizeof(float) * 4 * NUM_MODELS);
    memset(temp_age, 0, sizeof(float) * 4 * NUM_MODELS);
    memset(temp_var, 0, sizeof(float) * 4 * NUM_MODELS);

    // Horizontal Neighbor (left or right).
    //
    if (diAlloc[x] != 0) {
        int idx_new_i = idxNewIAlloc[x];
        int idx_new_j = idxNewJAlloc[x];
        idx_new_i += diAlloc[x] > 0 ? 1 : -1;
        if (idx_new_i >= 0 && idx_new_i < modelWidth && idx_new_j >= 0 && idx_new_j < modelHeight) {
            float w_H = fabs(diAlloc[x]) * (1.0f - fabs(djAlloc[x]));
            sumW += w_H;
            int idxNew = idx_new_i + idx_new_j * modelWidth;
            for (int m = 0; m < NUM_MODELS; ++m) {
                temp_mean[HORIZONTAL_NEIGHBOR][m] = w_H * meanAlloc[INDEX(m, idxNew)];
                temp_age[HORIZONTAL_NEIGHBOR][m] = w_H * ageAlloc[INDEX(m, idxNew)];
                temp_var[HORIZONTAL_NEIGHBOR][m] = w_H * (float)(varAlloc[INDEX(m, idxNew)] + VARIANCE_INTERPOLATE_PARAM * (pow(tempMeanAlloc[INDEX(m, x)] - meanAlloc[INDEX(m, idxNew)], 2)));
            }
        }
    }

    // Vertical Neighbor (up or down).
    //
    if (djAlloc[x] != 0) {
        int idx_new_i = idxNewIAlloc[x];
        int idx_new_j = idxNewJAlloc[x];
        idx_new_j += djAlloc[x] > 0 ? 1 : -1;
        if (idx_new_i >= 0 && idx_new_i < modelWidth && idx_new_j >= 0 && idx_new_j < modelHeight) {
            float w_V = fabs(djAlloc[x]) * (1.0f - fabs(diAlloc[x]));
            sumW += w_V;
            int idxNew = idx_new_i + idx_new_j * modelWidth;
            for (int m = 0; m < NUM_MODELS; ++m) {
                temp_mean[VERTICAL_NEIGHBOR][m] = w_V * meanAlloc[INDEX(m, idxNew)];
                temp_age[VERTICAL_NEIGHBOR][m] = w_V * ageAlloc[INDEX(m, idxNew)];
                temp_var[VERTICAL_NEIGHBOR][m] = w_V * (float)(varAlloc[INDEX(m, idxNew)] + VARIANCE_INTERPOLATE_PARAM * (pow(tempMeanAlloc[INDEX(m, x)] - meanAlloc[INDEX(m, idxNew)], 2)));
            }
        }
    }

    // Diagonal neighbor (up-left, up-right, down-left or down-right).
    //
    if (djAlloc[x] != 0 && diAlloc[x] != 0) {
        int idx_new_i = idxNewIAlloc[x];
        int idx_new_j = idxNewJAlloc[x];
        idx_new_i += diAlloc[x] > 0 ? 1 : -1;
        idx_new_j += djAlloc[x] > 0 ? 1 : -1;
        if (idx_new_i >= 0 && idx_new_i < modelWidth && idx_new_j >= 0 && idx_new_j < modelHeight) {
            float w_HV = fabs(diAlloc[x]) * fabs(djAlloc[x]);
            sumW += w_HV;
            int idxNew = idx_new_i + idx_new_j * modelWidth;
            for (int m = 0; m < NUM_MODELS; ++m) {
                temp_mean[DIAGONAL_NEIGHBOR][m] = w_HV * meanAlloc[INDEX(m, idxNew)];
                temp_age[DIAGONAL_NEIGHBOR][m] = w_HV * ageAlloc[INDEX(m, idxNew)];
                temp_var[DIAGONAL_NEIGHBOR][m] = w_HV * (float)(varAlloc[INDEX(m, idxNew)] + VARIANCE_INTERPOLATE_PARAM * (pow(tempMeanAlloc[INDEX(m, x)] - meanAlloc[INDEX(m, idxNew)], 2)));
            }
        }
    }

    // Self
    //
    if (idxNewIAlloc[x] >= 0 && idxNewIAlloc[x] < modelWidth && idxNewJAlloc[x] >= 0 && idxNewJAlloc[x] < modelHeight) {
        float w_self = (1.0f - fabs(diAlloc[x])) * (1.0f - fabs(djAlloc[x]));
        sumW += w_self;
        int idxNew = idxNewIAlloc[x] + idxNewJAlloc[x] * modelWidth;
        for (int m = 0; m < NUM_MODELS; ++m) {
            temp_mean[SELF][m] = w_self * meanAlloc[INDEX(m, idxNew)];
            temp_age[SELF][m] = w_self * ageAlloc[INDEX(m, idxNew)];
            temp_var[SELF][m] = w_self * (float)(varAlloc[INDEX(m, idxNew)] + VARIANCE_INTERPOLATE_PARAM * (pow(tempMeanAlloc[INDEX(m, x)] - meanAlloc[INDEX(m, idxNew)], 2)));
        }
    }

    // Do not compare to 0 because of floating point inaccuracy.
    //
    if (sumW > 0.001f) {
        for (int m = 0; m < NUM_MODELS; ++m) {
            tempMeanAlloc[INDEX(m, x)] = (temp_mean[HORIZONTAL_NEIGHBOR][m] + temp_mean[VERTICAL_NEIGHBOR][m] + temp_mean[DIAGONAL_NEIGHBOR][m] + temp_mean[SELF][m]) / sumW;
            tempAgeAlloc[INDEX(m, x)] = (temp_age[HORIZONTAL_NEIGHBOR][m] + temp_age[VERTICAL_NEIGHBOR][m] + temp_age[DIAGONAL_NEIGHBOR][m] + temp_age[SELF][m]) / sumW;
            tempVarAlloc[INDEX(m, x)] = (temp_var[HORIZONTAL_NEIGHBOR][m] + temp_var[VERTICAL_NEIGHBOR][m] + temp_var[DIAGONAL_NEIGHBOR][m] + temp_var[SELF][m]) / sumW;
        }
    }

    // Limitations and Exceptions
    for (int m = 0; m < NUM_MODELS; ++m) {
        tempVarAlloc[INDEX(m, x)] = MAX(tempVarAlloc[INDEX(m, x)], MIN_BG_VAR);
    }
    if (idxNewIAlloc[x] < 1 || idxNewIAlloc[x] >= modelWidth - 1 || idxNewJAlloc[x] < 1 || idxNewJAlloc[x] >= modelHeight - 1) {
        for (int m = 0; m < NUM_MODELS; ++m) {
            tempVarAlloc[INDEX(m, x)] = INIT_BG_VAR;
            tempAgeAlloc[INDEX(m, x)] = 0;
        }
    } else {
        for (int m = 0; m < NUM_MODELS; ++m) {
            tempAgeAlloc[INDEX(m, x)] =
                    (float)MIN(tempAgeAlloc[INDEX(m, x)] * exp(-VAR_DEC_RATIO * MAX(0.0f, tempVarAlloc[INDEX(m, x)] - VAR_MIN_NOISE_T)), MAX_BG_AGE);
        }
    }
}

void RS_KERNEL updateTempParamModel(int32_t in, uint32_t x) {
    uint32_t bIdx_j = x / modelWidth;
    uint32_t bIdx_i = x - bIdx_j * modelWidth;

    const int idx_base_i = bIdx_i * BLOCK_SIZE;
    const int idx_base_j = bIdx_j * BLOCK_SIZE;

    modelIdxAlloc[x] = 0;

    ////////////////////////////////////////////////////////////////////////////////
    // Match model
    //
    float cur_mean = 0;
    int elem_cnt = 0;

    // Find mean average for this block.
    //
    for (int jj = 0; jj < BLOCK_SIZE; ++jj) {
        for (int ii = 0; ii < BLOCK_SIZE; ++ii) {

            int idx_i = idx_base_i + ii;
            int idx_j = idx_base_j + jj;

            if (idx_i < 0 || idx_i >= inputImageCols || idx_j < 0 || idx_j >= inputImageRows)
                continue;

            cur_mean += pCur[idx_i + idx_j * inputImageCols];
            elem_cnt++;
        }
    }	//loop for pixels
    cur_mean /= elem_cnt;

    // Make Oldest Idx to 0 (swap)
    //
    int oldIdx = 0;
    float oldAge = 0;
    for (int m = 0; m < NUM_MODELS; ++m) {
        float fAge = tempAgeAlloc[INDEX(m, x)];

        if (fAge >= oldAge) {
            oldIdx = m;
            oldAge = fAge;
        }
    }

    if (oldIdx != 0) {
        tempMeanAlloc[INDEX(0, x)] = tempMeanAlloc[INDEX(oldIdx, x)];
        tempMeanAlloc[INDEX(oldIdx, x)] = cur_mean;

        tempVarAlloc[INDEX(0, x)] = tempVarAlloc[INDEX(oldIdx, x)];
        tempVarAlloc[INDEX(oldIdx, x)] = INIT_BG_VAR;

        tempAgeAlloc[INDEX(0, x)] = tempAgeAlloc[INDEX(oldIdx, x)];
        tempAgeAlloc[INDEX(oldIdx, x)] = 0;
    }

    // Select Model
    // Check Match against 0
    //
    if (pow(cur_mean - tempMeanAlloc[INDEX(0, x)], 2) < VAR_THRESH_MODEL_MATCH * tempVarAlloc[INDEX(0, x)]) {
        modelIdxAlloc[x] = 0;
    }
    // Check Match against 1
    else if (pow(cur_mean - tempMeanAlloc[INDEX(1, x)], 2) < VAR_THRESH_MODEL_MATCH * tempVarAlloc[INDEX(1, x)]) {
        modelIdxAlloc[x] = 1;
    }
    // If No match, set 1 age to zero and match = 1
    else {
        modelIdxAlloc[x] = 1;
        tempAgeAlloc[INDEX(1, x)] = 0;
    }
}

void RS_KERNEL updateMeanVarAgeOutput(int32_t in, uint32_t x) {
    uint32_t bIdx_j = x / modelWidth;
    uint32_t bIdx_i = x - bIdx_j * modelWidth;

    const int idx_base_i = bIdx_i * BLOCK_SIZE;
    const int idx_base_j = bIdx_j * BLOCK_SIZE;

    ////////////////////////////////////////////////////////////////////////////////
    // Update mean, variance, and age
    //
    int nMatchIdx = modelIdxAlloc[x];

    // obtain observation mean
    int nElemCnt[NUM_MODELS];
    float obs_mean[NUM_MODELS];
    float obs_var[NUM_MODELS];

    memset(nElemCnt, 0, sizeof(int) * NUM_MODELS);
    memset(obs_mean, 0, sizeof(float) * NUM_MODELS);
    memset(obs_var, 0, sizeof(float) * NUM_MODELS);

    float distImg;

    for (int jj = 0; jj < BLOCK_SIZE; ++jj) {
        for (int ii = 0; ii < BLOCK_SIZE; ++ii) {

            int idx_i = idx_base_i + ii;
            int idx_j = idx_base_j + jj;

            if (idx_i < 0 || idx_i >= inputImageCols || idx_j < 0 || idx_j >= inputImageRows)
                continue;

            int index = idx_i + idx_j * inputImageCols;

            obs_mean[nMatchIdx] += pCur[idx_i + idx_j * inputImageCols];

            float pixelDist = 0.0;
            float fDiff = pCur[index] - meanAlloc[INDEX(nMatchIdx, x)];
            pixelDist += pow(fDiff, (int)2);

            distImg = (float)pow(pCur[index] - meanAlloc[INDEX(0, x)], 2);

            if (tempAgeAlloc[INDEX(0, x)] > 1) {
                uchar valOut = distImg > VAR_THRESH_FG_DETERMINE * tempVarAlloc[INDEX(0, x)] ? (uchar)255 : (uchar)0;

            #ifndef TEST_ENV
                if (valOut > 0) {
                    pInputImg[index].r = 255;
                    pInputImg[index].g = (pInputImg[index].g * 0.5f) + 128;
                    pInputImg[index].b = (pInputImg[index].b * 0.5f) + 128;
                }
            #else
                if (valOut == 0) {
                    pInputImg[index].r = 0;
                    pInputImg[index].g = 0;
                    pInputImg[index].b = 0;
                }
            #endif
            }
        #ifdef TEST_ENV
            else {
                pInputImg[index].b = 0;
                pInputImg[index].g = 0;
                pInputImg[index].r = 0;
            }
        #endif

            obs_var[nMatchIdx] = MAX(obs_var[nMatchIdx], pixelDist);

            nElemCnt[nMatchIdx]++;
        }
    }

    for (int m = 0; m < NUM_MODELS; ++m) {

        if (nElemCnt[m] <= 0) {
            meanAlloc[INDEX(m, x)] = tempMeanAlloc[INDEX(m, x)];
            varAlloc[INDEX(m, x)] = tempVarAlloc[INDEX(m, x)];
            ageAlloc[INDEX(m, x)] = tempAgeAlloc[INDEX(m, x)];
        } else {
            // learning rate for this block
            float age = tempAgeAlloc[INDEX(m, x)];
            float alpha = age / (age + 1.0f);

            obs_mean[m] /= ((float)nElemCnt[m]);

            // Update with this mean and variance
            if (age < 1) {
                meanAlloc[INDEX(m, x)] = obs_mean[m];
                varAlloc[INDEX(m, x)] = MAX(obs_var[m], INIT_BG_VAR);
            } else {
                meanAlloc[INDEX(m, x)] = alpha * tempMeanAlloc[INDEX(m, x)] + (1.0f - alpha) * obs_mean[m];
                varAlloc[INDEX(m, x)] = alpha * tempVarAlloc[INDEX(m, x)] + (1.0f - alpha) * obs_var[m];
                varAlloc[INDEX(m, x)] = MAX(varAlloc[INDEX(m, x)], MIN_BG_VAR);
            }

            // Update Age
            ageAlloc[INDEX(m, x)] = tempAgeAlloc[INDEX(m, x)] + 1.0f;
            ageAlloc[INDEX(m, x)] = MIN(ageAlloc[INDEX(m, x)], MAX_BG_AGE);
        }
    }
}