#pragma once

#define BLOCK_SIZE	(4)
#define HalfBlockSize (2);

#define VARIANCE_INTERPOLATE_PARAM	        (1.0f)

#define MAX_BG_AGE				(30.0f)
#define VAR_MIN_NOISE_T			        (50.0f*50.0f)
#define VAR_DEC_RATIO			        (0.001f)
#define MIN_BG_VAR				(5.0f*5.0f)	//15*15
#define INIT_BG_VAR				(20.0f*20.0f)	//15*15

#define NUM_MODELS		        (2)
#define VAR_THRESH_FG_DETERMINE		(4.0f)
#define VAR_THRESH_MODEL_MATCH		(2.0f)

#define INDEX(a, b) (a * blockCount + b)

#ifndef MIN
#  define MIN(a,b)  ((a) > (b) ? (b) : (a))
#endif

#ifndef MAX
#  define MAX(a,b)  ((a) < (b) ? (b) : (a))
#endif

#define TEST_ENV

typedef unsigned char BYTE;

enum NeighborIndex {
    HORIZONTAL_NEIGHBOR =0,
    VERTICAL_NEIGHBOR,
    DIAGONAL_NEIGHBOR,
    SELF,
    TOTAL_NEIGHBORS = SELF + 1,
};