//
// Created by Petar on 03.05.2019..
//

#pragma once

class Dsp {
private:
    const char *appCacheDir;

public:
    Dsp(const char *app_cache_dir) {
        appCacheDir = app_cache_dir;
    }

    void call_rs();

};
