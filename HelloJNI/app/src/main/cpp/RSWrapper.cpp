//
// Created by Petar on 04.05.2019..
//

#include "RSWrapper.hpp"
#include <android/log.h>
#include <string>
#include "params.hpp"

using namespace android::RSC;

RSWrapper::RSWrapper(const char *appCacheDir)
{
    this->m_RS = new RS();
    m_RS->init(appCacheDir);

    m_scriptC_probmodel = new ScriptC_probmodel(m_RS);
    m_homographyMatrix = Allocation::createSized(m_RS, Element::F32(m_RS), 9);

    m_scriptC_probmodel->bind_homographyMatrix(m_homographyMatrix);
}

RSWrapper::~RSWrapper()
{
    delete m_scriptC_probmodel;
}

//---------------------------------------------------
// SetHomographyMatrix
//  Copies passed value to the homography allocation.
//  Allocation is bound to the script global variable,
//  and is visible from there.
//
void RSWrapper::SetHomographyMatrix(const float h[9]) {
    m_homographyMatrix->copy1DFrom(h);
}

void RSWrapper::SetPCurData(unsigned char *pCur) {
    m_pCurAlloc->copy1DFrom(pCur);
}

//---------------------------------------------------
// SetModelDimensions
//  Initializes model width and height inside the script.
//
void RSWrapper::SetModelDimensions(int modelWidth, int modelHeight){
    modelSize = modelHeight * modelWidth;
    pOut = new unsigned char[modelSize * BLOCK_SIZE * BLOCK_SIZE];

    m_scriptC_probmodel->set_modelWidth(modelWidth);
    m_scriptC_probmodel->set_modelHeight(modelHeight);
    m_scriptC_probmodel->set_blockCount(modelSize);
    m_scriptC_probmodel->set_inputImageCols(modelWidth * BLOCK_SIZE);
    m_scriptC_probmodel->set_inputImageRows(modelHeight * BLOCK_SIZE);

    __android_log_print(ANDROID_LOG_INFO, "DebugTag", "model width: %d, model height %d", modelWidth, modelHeight);
    BindAllocations();
}

void RSWrapper::CalculateIndices(){
    m_scriptC_probmodel->forEach_calculatePositionOffset(m_idxNow);
}

void RSWrapper::MotionCompensate() {
    m_diAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_djAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_idxNewIAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_idxNewJAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_MeanAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_VarAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_AgeAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);

    m_scriptC_probmodel->forEach_motionCompensate(m_idxNow);
}

void RSWrapper::UpdateTempParamModel() {
    m_tempMeanAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_tempAgeAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_tempVarAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);

    m_scriptC_probmodel->forEach_updateTempParamModel(m_idxNow);
}

void RSWrapper::UpdateMeanVarAgeOutput() {
    m_tempMeanAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_tempAgeAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_tempVarAlloc->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_idxNow->syncAll(RS_ALLOCATION_USAGE_SCRIPT);

    m_scriptC_probmodel->forEach_updateMeanVarAgeOutput(m_idxNow);
}

void RSWrapper::CopyInput(unsigned char* inputImgData){
    m_pInputImage->copy1DFrom(inputImgData);
}

void RSWrapper::DisplayResult(unsigned char *destinationData) {
    m_pInputImage->syncAll(RS_ALLOCATION_USAGE_SCRIPT);
    m_pInputImage->copy1DTo(destinationData);
}

void RSWrapper::Test(){
    float h[9];
    m_scriptC_probmodel->forEach_dummyKernel(m_homographyMatrix);
    m_homographyMatrix->copy1DTo(h);
    std::string result = "";
    for (auto hh : h) result += " " + std::to_string(hh);
    __android_log_print(ANDROID_LOG_INFO, "DebugTag", "h: %s.", result.c_str());
}

void RSWrapper::BindAllocations(){
    m_idxNow = Allocation::createSized(m_RS, Element::I32(m_RS), modelSize, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_probmodel->bind_modelIdxAlloc(m_idxNow);

    // Create and bind indices allocation.
    //
    m_diAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize, RS_ALLOCATION_USAGE_SCRIPT);
    m_djAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize, RS_ALLOCATION_USAGE_SCRIPT);
    m_idxNewIAlloc = Allocation::createSized(m_RS, Element::I32(m_RS), modelSize, RS_ALLOCATION_USAGE_SCRIPT);
    m_idxNewJAlloc = Allocation::createSized(m_RS, Element::I32(m_RS), modelSize, RS_ALLOCATION_USAGE_SCRIPT);

    m_scriptC_probmodel->bind_diAlloc(m_diAlloc);
    m_scriptC_probmodel->bind_djAlloc(m_djAlloc);
    m_scriptC_probmodel->bind_idxNewIAlloc(m_idxNewIAlloc);
    m_scriptC_probmodel->bind_idxNewJAlloc(m_idxNewJAlloc);

    // Create and bind parameter allocations (mean, age, var)
    //
    m_MeanAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_tempMeanAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_probmodel->bind_meanAlloc(m_MeanAlloc);
    m_scriptC_probmodel->bind_tempMeanAlloc(m_tempMeanAlloc);

    m_AgeAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_tempAgeAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_probmodel->bind_ageAlloc(m_AgeAlloc);
    m_scriptC_probmodel->bind_tempAgeAlloc(m_tempAgeAlloc);

    m_VarAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_tempVarAlloc = Allocation::createSized(m_RS, Element::F32(m_RS), modelSize * NUM_MODELS, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_probmodel->bind_varAlloc(m_VarAlloc);
    m_scriptC_probmodel->bind_tempVarAlloc(m_tempVarAlloc);

    // Create and bind image pixel data info
    //
    m_pCurAlloc = Allocation::createSized(m_RS, Element::U8(m_RS), modelSize * BLOCK_SIZE * BLOCK_SIZE, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_probmodel->bind_pCur(m_pCurAlloc);
    m_pInputImage = Allocation::createSized(m_RS, Element::U8_4(m_RS),  modelSize * BLOCK_SIZE * BLOCK_SIZE, RS_ALLOCATION_USAGE_SCRIPT);
    m_scriptC_probmodel->bind_pInputImg(m_pInputImage);
}
