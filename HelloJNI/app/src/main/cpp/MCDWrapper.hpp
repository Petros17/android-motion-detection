#pragma once

// Includes for this wrapper
#include "KLTWrapper.hpp"
#include "prob_model.hpp"

class MCDWrapper {

 private:
    cv::Mat imgTemp;
	cv::Mat imgGray;
    cv::Mat detect_img;

    KLTWrapper m_LucasKanade;
    ProbModel BGModel;

 public:
    BYTE* DetectImgData() const { return detect_img.data; }

	void Init(const cv::Mat &in_imgIpl);
	void Run(cv::Mat &in);
	void InitProbModelCacheDir(const char* appCacheDir);

private:
	void CopyInputToRS(const cv::Mat &in);
};
