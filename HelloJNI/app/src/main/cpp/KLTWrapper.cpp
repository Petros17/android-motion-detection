#include "KLTWrapper.hpp"

#include <vector>
#include <string>

#include <opencv2/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/video.hpp>
#include <android/log.h>

cv::TermCriteria termCriteria(cv::TermCriteria::COUNT|cv::TermCriteria::EPS,20,0.03);

KLTWrapper::KLTWrapper()
{
    matH = new double[9];
}

KLTWrapper::~KLTWrapper()
{
    delete[] matH;
}

void KLTWrapper::Init(cv::Mat const &imgGray)
{
	int ni = imgGray.cols;
	int nj = imgGray.rows;

	// Allocate Maximum possible + some more for safety
	MAX_COUNT = (float (ni) / float (GRID_SIZE_W) + 1.0)*(float (nj) / float (GRID_SIZE_H) + 1.0);

	defaultCount = ni / GRID_SIZE_W * nj / GRID_SIZE_H;

    for (int i = 0; i < ni / GRID_SIZE_W - 1; i++)
    for (int j = 0; j < nj / GRID_SIZE_H - 1; j++){
        cv::Point2f point(i * GRID_SIZE_W + GRID_SIZE_W / 2, j * GRID_SIZE_H + GRID_SIZE_H / 2);
        defaultPoints.emplace_back(point);
    }

	// Init homography
	for (int i = 0; i < 9; i++)
		matH[i] = i / 3 == i % 3 ? 1 : 0;
}

void KLTWrapper::InitFeatures(cv::Mat const &imgGray)
{
    points[1].clear();
    points[1] = defaultPoints;
    points[1].resize(defaultPoints.size());
    count = defaultCount;
}

void KLTWrapper::RunTrack(cv::Mat const &imgGray)
{
	int i, k;
	std::unique_ptr<int[]> nMatch(new int[MAX_COUNT]);

    if (imgPrevGray.empty()) {
        imgGray.copyTo(imgPrevGray);
    }

    if (count > 0){
        std::vector<float> err;

        cv::calcOpticalFlowPyrLK(imgPrevGray, imgGray, points[0], points[1], status, err,
                                 cv::Size(win_size, win_size), 3, termCriteria);

        for (i = k = 0; i < status.size(); i++) {
            if (!status[i])
                continue;

            nMatch[k++] = i;
        }
        count = k;
    }

    if (count >= 10) {
        // Make homography matrix with correspondences
        MakeHomography(nMatch.get());
    } else {
        for (int ii = 0; ii < 9; ++ii) {
            matH[ii] = ii % 3 == ii / 3 ? 1.0f : 0.0f;
        }
    }
    InitFeatures(imgGray);
	SwapData(imgGray);
}

void KLTWrapper::SwapData(cv::Mat const &imgGray)
{
	imgGray.copyTo(imgPrevGray);
	std::swap(points[1], points[0]);
}

void KLTWrapper::MakeHomography(const int *nMatch)
{
    std::vector<cv::Point2f> pt1, pt2;

    pt1.resize(count);
    pt2.resize(count);
    for (int i = 0; i < count; i++){
        pt1[i] = points[1][nMatch[i]];
        pt2[i] = points[0][nMatch[i]];
    }

    cv::Mat pt1Mat(pt1);
    cv::Mat pt2Mat(pt2);

    cv::Mat _hMat = cv::findHomography(pt1Mat, pt2Mat, CV_RANSAC, 1);
    if (!_hMat.empty()) {
        for (int i = 0; i < 9; i++) {
            matH[i] = _hMat.at<double>(i);
        }
    }
}
