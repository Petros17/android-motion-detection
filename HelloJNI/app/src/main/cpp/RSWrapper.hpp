//
// Created by Petar on 04.05.2019..
//

#pragma once

#include <RenderScript.h>
#include <ScriptC_probmodel.h>

using namespace android::RSC;

class RSWrapper {
private:
    sp<RS> m_RS;
    ScriptC_probmodel* m_scriptC_probmodel;

    unsigned int modelSize;
    unsigned char* pOut;

    // Allocations linked to RS
    //
    sp<Allocation> m_homographyMatrix;
    sp<Allocation> m_diAlloc;
    sp<Allocation> m_djAlloc;
    sp<Allocation> m_idxNewIAlloc;
    sp<Allocation> m_idxNewJAlloc;

    // Dummy alloc used to initiate iterating through all blocks.
    sp<Allocation> m_idxNow;

    sp<Allocation> m_tempMeanAlloc;
    sp<Allocation> m_MeanAlloc;
    sp<Allocation> m_tempAgeAlloc;
    sp<Allocation> m_AgeAlloc;
    sp<Allocation> m_tempVarAlloc;
    sp<Allocation> m_VarAlloc;

    sp<Allocation> m_pCurAlloc;

    sp<Allocation> m_pInputImage;

public:
    explicit RSWrapper(const char *appCacheDir);
    ~RSWrapper();

    void SetHomographyMatrix(const float h[9]);
    void SetPCurData(unsigned char* pCur);

    void SetModelDimensions(int modelWidth, int modelHeight);

    void CalculateIndices();
    void MotionCompensate();
    void UpdateTempParamModel();
    void UpdateMeanVarAgeOutput();

    void CopyInput(unsigned char* inputImgData);

    void DisplayResult(unsigned char* destinationData);

    void Test();

private:
    void BindAllocations();
};

