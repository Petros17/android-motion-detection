//
// Created by petark on 3/4/2019.
//

#include <jni.h>
#include <opencv2/opencv.hpp>
#include "main.hpp"
#include <android/log.h>

extern "C" {
    jint
    Java_com_example_hellojni_HelloJni_imgProcess(JNIEnv *env, jobject, jlong inputMat){
        cv::Mat &mrgb = *(cv::Mat*) inputMat;
        jint retVal = (jint) doIt(mrgb);
        return retVal;
    }

    void
    Java_com_example_hellojni_HelloJni_initCacheDir(JNIEnv *env, jobject, jstring cachePath){
        initCacheDir(env->GetStringUTFChars(cachePath, nullptr));
    }
}
