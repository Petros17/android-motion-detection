#pragma once

#include <opencv2\opencv.hpp>

#define GRID_SIZE_W (32)
#define GRID_SIZE_H (24)

typedef unsigned char BYTE;

class KLTWrapper {
 private:
	// For LK
	cv::Mat imgPrevGray;

    std::vector<cv::Point2f> points[2];
    std::vector<cv::Point2f> defaultPoints;
	std::vector<uchar> status;

	int count = 0;
	int defaultCount;
    int win_size = 10;
    int MAX_COUNT;

	// For Homography Matrix
    double* matH;

 private:
	void SwapData(cv::Mat const &imgGray);
	void MakeHomography(const int *nMatch);

 public:
    KLTWrapper();
    ~KLTWrapper();
	void Init(cv::Mat const &imgGray);
	void InitFeatures(cv::Mat const &imgGray);
	void RunTrack(cv::Mat const &imgGray);	// with MakeHomography
	double* GetHomography() const { return matH;}
};
