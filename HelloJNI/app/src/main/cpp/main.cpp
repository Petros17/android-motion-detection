#include "main.hpp"
#include "MCDWrapper.hpp"
#include <android/log.h>
#include <cinttypes>

static int frame_count = 1;
static MCDWrapper *mcdWrapper = new MCDWrapper();

int64_t getTimeNsec() {
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    return (int64_t) now.tv_sec*1000000000LL + now.tv_nsec;
}

void initCacheDir(const char* appCacheDir) {
	mcdWrapper->InitProbModelCacheDir(appCacheDir);
}

int doIt(cv::Mat& img)
{
	if (1 == frame_count) {
		// Init the wrapper for first frame
		mcdWrapper->Init(img);
	} else {
	    int64_t startNss = getTimeNsec();
        // Run detection
		mcdWrapper->Run(img);
		int64_t endNss = getTimeNsec();

		int64_t nsDiff = (endNss - startNss);
		float fps = (1000000000.0f/nsDiff);
        __android_log_print(ANDROID_LOG_INFO, "DebugTag", "Execution time: %" PRId64 ", FPS: %.3f\n", nsDiff, fps);
	}

	frame_count++;

	return 0;
}