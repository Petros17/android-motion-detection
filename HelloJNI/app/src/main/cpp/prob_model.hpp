#pragma once

#include <vector>
#include <algorithm>
#include <opencv2/imgproc.hpp>
#include "opencv2\opencv.hpp"
#include "params.hpp"
#include "KLTWrapper.hpp"
#include "RSWrapper.hpp"

//#define SEQUENTIAL
#define OPTIMIZED_UPDATE

class ProbModel {
 private:
	cv::Mat m_Cur;
	std::vector<float> m_DistImg;
    int* m_ModelIdx;

    float* m_Mean;
    float* m_Var;
	float* m_Age;

    float* m_Mean_Temp;
    float* m_Var_Temp;
    float* m_Age_Temp;

	int modelWidth = 0;
	int modelHeight = 0;
    unsigned int blockCount = 0;

	int inputImageCols = 0;
	int inputImageRows = 0;

	std::unique_ptr<RSWrapper> m_rsWrapper;

 public:
	 ProbModel() =default;
	~ProbModel();
	
	void Init(cv::Mat const &inputImg);
	void MotionCompensate(const double h[9], cv::Mat const &curImg);
	void Update(cv::Mat &pOutputImg);
	void InitRenderScript(const char* appCacheDir);

	void CopyInputToRS(const cv::Mat& inputImg);
	void DisplayResult(cv::Mat& inputImg);

private:
    void MotionCompensateSequential(const double h[9], cv::Mat const &curImg);
    void MotionCompensateParallel(const double h[9], cv::Mat const &curImg);
    void UpdateSequential(cv::Mat &pOutputImg);
    void UpdateParallel();
};
