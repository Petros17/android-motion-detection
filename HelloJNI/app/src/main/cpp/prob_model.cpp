//
// Created by Petar on 23.03.2019..
//

#include "prob_model.hpp"
#include "KLTWrapper.hpp"
#include <android/log.h>
#include <memory>

#define INDEX(m, idx) (m * blockCount + idx)

enum NeighborIndex {
    HORIZONTAL_NEIGHBOR =0,
    VERTICAL_NEIGHBOR,
    DIAGONAL_NEIGHBOR,
    SELF,
    TOTAL_NEIGHBORS = SELF + 1,
};

ProbModel::~ProbModel() {
    delete[] m_Mean;
    delete[] m_Age;
    delete[] m_Var;
    delete[] m_Mean_Temp;
    delete[] m_Age_Temp;
    delete[] m_Var_Temp;
    delete[] m_ModelIdx;
}

void ProbModel::Init(cv::Mat const &inputImg) {

    inputImg.copyTo(m_Cur);

    inputImageCols = inputImg.cols;
    inputImageRows = inputImg.rows;

    modelWidth = inputImageCols / BLOCK_SIZE;
    modelHeight = inputImageRows / BLOCK_SIZE;

    blockCount = (unsigned int)modelWidth * modelHeight;

    /////////////////////////////////////////////////////////////////////////////
    // Initialize Storage
    m_DistImg = std::vector<float>((unsigned int)inputImageCols * inputImageRows, 0);
    m_ModelIdx = new int[blockCount];
    memset(m_ModelIdx, 0, blockCount * sizeof(int));

    m_Mean = new float[NUM_MODELS * blockCount];
    m_Var = new float[NUM_MODELS * blockCount];
    m_Age = new float[NUM_MODELS * blockCount];
    m_Mean_Temp = new float[NUM_MODELS * blockCount];
    m_Var_Temp = new float[NUM_MODELS * blockCount];
    m_Age_Temp = new float[NUM_MODELS * blockCount];

    m_rsWrapper->SetModelDimensions(modelWidth, modelHeight);

    // Update with homography I
    double h[9];
    h[0] = 1.0;
    h[1] = 0.0;
    h[2] = 0.0;
    h[3] = 0.0;
    h[4] = 1.0;
    h[5] = 0.0;
    h[6] = 0.0;
    h[7] = 0.0;
    h[8] = 1.0;

    MotionCompensate(h, inputImg);
    cv::Mat tempMat = cv::Mat();
    Update(tempMat);
}

void ProbModel::InitRenderScript(const char *appCacheDir) {
    m_rsWrapper = std::unique_ptr<RSWrapper>(new RSWrapper(appCacheDir));
    m_rsWrapper->Test();
}

void ProbModel::CopyInputToRS(const cv::Mat& inputImg) {
    m_rsWrapper->CopyInput(inputImg.data);
}

void ProbModel::DisplayResult(cv::Mat& inputImg) {
    m_rsWrapper->DisplayResult(inputImg.data);
}

void ProbModel::MotionCompensateSequential(const double hInput[9], cv::Mat const &curImg){
    float h[9];
    for (int i = 0; i < 9; i++) h[i] = static_cast<float>(hInput[i]);

    curImg.copyTo(m_Cur);
    int curModelWidth = modelWidth;
    int curModelHeight = modelHeight;

    // Pre-calculated to avoid re-calculation in every iteration.
    //
    const int HalfBlockSize = BLOCK_SIZE / 2;

    // compensate models for the current view
    for (int j = 0; j < curModelHeight; ++j) {
        for (int i = 0; i < curModelWidth; ++i) {

            // x and y coordinates for current model
            float blockCenterX = BLOCK_SIZE * i + HalfBlockSize;
            float blockCenterY = BLOCK_SIZE * j + HalfBlockSize;

            // transformed coordinates with h
            float newW = h[6] * blockCenterX + h[7] * blockCenterY + h[8];
            float newX = (h[0] * blockCenterX + h[1] * blockCenterY + h[2]) / newW;
            float newY = (h[3] * blockCenterX + h[4] * blockCenterY + h[5]) / newW;

            // transformed i,j coordinates of old position
            float newI = newX / BLOCK_SIZE;
            float newJ = newY / BLOCK_SIZE;

            int idxNewI = (int)floor(newI);
            int idxNewJ = (int)floor(newJ);

            float di = newI - (idxNewI + 0.5f);
            float dj = newJ - (idxNewJ + 0.5f);

            float sumW = 0.0;

            int idxNow = i + j * modelWidth;

            // For Mean, age, and variance
            {
                // Mean, age and variance for neighbors and self, for each model.
                //
                float temp_mean[TOTAL_NEIGHBORS][NUM_MODELS];
                float temp_age[TOTAL_NEIGHBORS][NUM_MODELS];
                float temp_var[TOTAL_NEIGHBORS][NUM_MODELS];
                memset(temp_mean, 0, sizeof(float) * 4 * NUM_MODELS);
                memset(temp_age, 0, sizeof(float) * 4 * NUM_MODELS);
                memset(temp_var, 0, sizeof(float) * 4 * NUM_MODELS);

                // Horizontal Neighbor (left or right).
                //
                if (di != 0) {
                    int idx_new_i = idxNewI;
                    int idx_new_j = idxNewJ;
                    idx_new_i += di > 0 ? 1 : -1;
                    if (idx_new_i >= 0 && idx_new_i < curModelWidth && idx_new_j >= 0 && idx_new_j < curModelHeight) {
                        float w_H = fabs(di) * (1.0f - fabs(dj));
                        sumW += w_H;
                        int idxNew = idx_new_i + idx_new_j * modelWidth;
                        for (int m = 0; m < NUM_MODELS; ++m) {
                            temp_mean[HORIZONTAL_NEIGHBOR][m] = w_H * m_Mean[INDEX(m, idxNew)];
                            temp_age[HORIZONTAL_NEIGHBOR][m] = w_H * m_Age[INDEX(m, idxNew)];
                            temp_var[HORIZONTAL_NEIGHBOR][m] = w_H * (float)(m_Var[INDEX(m, idxNew)] + VARIANCE_INTERPOLATE_PARAM * (pow(m_Mean_Temp[INDEX(m, idxNow)] - m_Mean[INDEX(m, idxNew)], 2)));
                        }
                    }
                }

                // Vertical Neighbor (up or down).
                //
                if (dj != 0) {
                    int idx_new_i = idxNewI;
                    int idx_new_j = idxNewJ;
                    idx_new_j += dj > 0 ? 1 : -1;
                    if (idx_new_i >= 0 && idx_new_i < curModelWidth && idx_new_j >= 0 && idx_new_j < curModelHeight) {
                        float w_V = fabs(dj) * (1.0f - fabs(di));
                        sumW += w_V;
                        int idxNew = idx_new_i + idx_new_j * modelWidth;
                        for (int m = 0; m < NUM_MODELS; ++m) {
                            temp_mean[VERTICAL_NEIGHBOR][m] = w_V * m_Mean[m * blockCount + idxNew];
                            temp_age[VERTICAL_NEIGHBOR][m] = w_V * m_Age[m * blockCount + idxNew];
                            temp_var[VERTICAL_NEIGHBOR][m] = w_V * (float)(m_Var[m * blockCount + idxNew] + VARIANCE_INTERPOLATE_PARAM * (pow(m_Mean_Temp[INDEX(m, idxNow)] - m_Mean[INDEX(m, idxNew)], 2)));
                        }
                    }
                }
                // Diagonal neighbor (up-left, up-right, down-left or down-right).
                //
                if (dj != 0 && di != 0) {
                    int idx_new_i = idxNewI;
                    int idx_new_j = idxNewJ;
                    idx_new_i += di > 0 ? 1 : -1;
                    idx_new_j += dj > 0 ? 1 : -1;
                    if (idx_new_i >= 0 && idx_new_i < curModelWidth && idx_new_j >= 0 && idx_new_j < curModelHeight) {
                        float w_HV = fabs(di) * fabs(dj);
                        sumW += w_HV;
                        int idxNew = idx_new_i + idx_new_j * modelWidth;
                        for (int m = 0; m < NUM_MODELS; ++m) {
                            temp_mean[DIAGONAL_NEIGHBOR][m] = w_HV * m_Mean[INDEX(m, idxNew)];
                            temp_age[DIAGONAL_NEIGHBOR][m] = w_HV * m_Age[INDEX(m, idxNew)];
                            temp_var[DIAGONAL_NEIGHBOR][m] = w_HV * (float)(m_Var[INDEX(m, idxNew)] + VARIANCE_INTERPOLATE_PARAM * (pow(m_Mean_Temp[INDEX(m, idxNow)] - m_Mean[INDEX(m, idxNew)], 2)));
                        }
                    }
                }

                // Self
                //
                if (idxNewI >= 0 && idxNewI < curModelWidth && idxNewJ >= 0 && idxNewJ < curModelHeight) {
                    float w_self = (1.0f - fabs(di)) * (1.0f - fabs(dj));
                    sumW += w_self;
                    int idxNew = idxNewI + idxNewJ * modelWidth;
                    for (int m = 0; m < NUM_MODELS; ++m) {
                        temp_mean[SELF][m] = w_self * m_Mean[INDEX(m, idxNew)];
                        temp_age[SELF][m] = w_self * m_Age[INDEX(m, idxNew)];
                        temp_var[SELF][m] = w_self * (float)(m_Var[INDEX(m, idxNew)] + VARIANCE_INTERPOLATE_PARAM * (pow(m_Mean_Temp[INDEX(m, idxNow)] - m_Mean[INDEX(m, idxNew)], 2)));
                    }
                }

                if (sumW > 0) {
                    for (int m = 0; m < NUM_MODELS; ++m) {
                        m_Mean_Temp[INDEX(m, idxNow)] = (temp_mean[HORIZONTAL_NEIGHBOR][m] + temp_mean[VERTICAL_NEIGHBOR][m] + temp_mean[DIAGONAL_NEIGHBOR][m] + temp_mean[SELF][m]) / sumW;
                        m_Age_Temp[INDEX(m, idxNow)] = (temp_age[HORIZONTAL_NEIGHBOR][m] + temp_age[1][VERTICAL_NEIGHBOR] + temp_age[DIAGONAL_NEIGHBOR][m] + temp_age[SELF][m]) / sumW;
                        m_Var_Temp[INDEX(m, idxNow)] = (temp_var[HORIZONTAL_NEIGHBOR][m] + temp_var[1][VERTICAL_NEIGHBOR] + temp_var[DIAGONAL_NEIGHBOR][m] + temp_var[SELF][m]) / sumW;
                    }
                }
            }

            // Limitations and Exceptions
            for (int m = 0; m < NUM_MODELS; ++m) {
                m_Var_Temp[INDEX(m, i + j * modelWidth)] = MAX(m_Var_Temp[INDEX(m, i + j * modelWidth)], MIN_BG_VAR);
            }
            if (idxNewI < 1 || idxNewI >= modelWidth - 1 || idxNewJ < 1 || idxNewJ >= modelHeight - 1) {
                for (int m = 0; m < NUM_MODELS; ++m) {
                    m_Var_Temp[INDEX(m, i + j * modelWidth)] = INIT_BG_VAR;
                    m_Age_Temp[INDEX(m, i + j * modelWidth)] = 0;
                }
            } else {
                for (int m = 0; m < NUM_MODELS; ++m) {
                    m_Age_Temp[INDEX(m, i + j * modelWidth)] =
                            (float)MIN(m_Age_Temp[INDEX(m, i + j * modelWidth)] * exp(-VAR_DEC_RATIO * MAX(0.0, m_Var_Temp[INDEX(m, i + j * modelWidth)] - VAR_MIN_NOISE_T)), MAX_BG_AGE);
                }
            }
        }
    }

}

void ProbModel::MotionCompensateParallel(const double hInput[9], cv::Mat const &curImg) {
    float h[9];
    for (int i = 0; i < 9; i++) h[i] = static_cast<float>(hInput[i]);

    curImg.copyTo(m_Cur);
    m_rsWrapper->SetPCurData(curImg.data);

    m_rsWrapper->SetHomographyMatrix(h);
    m_rsWrapper->CalculateIndices();
    m_rsWrapper->MotionCompensate();
}

void ProbModel::MotionCompensate(const double *h, cv::Mat const &curImg) {
#ifdef SEQUENTIAL
    MotionCompensateSequential(h, curImg);
#else
    MotionCompensateParallel(h, curImg);
#endif
}

void ProbModel::Update(cv::Mat &pOutputImg){
#ifdef SEQUENTIAL
    UpdateSequential(pOutputImg);
#else
    UpdateParallel();
#endif
}

void ProbModel::UpdateParallel() {
    m_rsWrapper->UpdateTempParamModel();
    m_rsWrapper->UpdateMeanVarAgeOutput();
}

#ifdef OPTIMIZED_UPDATE
void ProbModel::UpdateSequential(cv::Mat &pOutputImg) {
    BYTE *pOut = pOutputImg.data;
    memset(m_ModelIdx, 0, blockCount * sizeof(int));

    BYTE *pCur = m_Cur.data;

    for (int bIdx_j = 0; bIdx_j < modelHeight; bIdx_j++) {
        for (int bIdx_i = 0; bIdx_i < modelWidth; bIdx_i++) {
            const int idx_base_i = bIdx_i * BLOCK_SIZE;
            const int idx_base_j = bIdx_j * BLOCK_SIZE;
            const int ArrayIndexingOffset = bIdx_i + bIdx_j * modelWidth;

            ////////////////////////////////////////////////////////////////////////////////
            // Match model
            //
            float cur_mean = 0;
            int elem_cnt = 0;
            // Find mean average for this block.
            //
            for (int jj = 0; jj < BLOCK_SIZE; ++jj) {
                for (int ii = 0; ii < BLOCK_SIZE; ++ii) {

                    int idx_i = idx_base_i + ii;
                    int idx_j = idx_base_j + jj;

                    if (idx_i < 0 || idx_i >= inputImageCols || idx_j < 0 || idx_j >= inputImageRows)
                        continue;

                    cur_mean += pCur[idx_i + idx_j * m_Cur.cols];
                    elem_cnt++;
                }
            }	//loop for pixels
            cur_mean /= elem_cnt;

            // Make Oldest Idx to 0 (swap)
            //
            int oldIdx = 0;
            float oldAge = 0;
            for (int m = 0; m < NUM_MODELS; ++m) {
                float fAge = m_Age_Temp[INDEX(m, bIdx_i + bIdx_j * modelWidth)];

                if (fAge >= oldAge) {
                    oldIdx = m;
                    oldAge = fAge;
                }
            }
            if (oldIdx != 0) {
                m_Mean_Temp[INDEX(0, ArrayIndexingOffset)] = m_Mean_Temp[INDEX(oldIdx, ArrayIndexingOffset)];
                m_Mean_Temp[INDEX(oldIdx, ArrayIndexingOffset)] = cur_mean;

                m_Var_Temp[INDEX(0, ArrayIndexingOffset)] = m_Var_Temp[INDEX(oldIdx, ArrayIndexingOffset)];
                m_Var_Temp[INDEX(oldIdx, ArrayIndexingOffset)] = INIT_BG_VAR;

                m_Age_Temp[INDEX(0, ArrayIndexingOffset)] = m_Age_Temp[INDEX(oldIdx, ArrayIndexingOffset)];
                m_Age_Temp[INDEX(oldIdx, ArrayIndexingOffset)] = 0;
            }

            // Select Model
            // Check Match against 0
            //
            if (pow(cur_mean - m_Mean_Temp[INDEX(0, ArrayIndexingOffset)], 2) < VAR_THRESH_MODEL_MATCH * m_Var_Temp[INDEX(0, ArrayIndexingOffset)]) {
                m_ModelIdx[ArrayIndexingOffset] = 0;
            }
                // Check Match against 1
            else if (pow(cur_mean - m_Mean_Temp[INDEX(1, ArrayIndexingOffset)], 2) < VAR_THRESH_MODEL_MATCH * m_Var_Temp[INDEX(1, ArrayIndexingOffset)]) {
                m_ModelIdx[ArrayIndexingOffset] = 1;
            }
                // If No match, set 1 age to zero and match = 1
            else {
                m_ModelIdx[ArrayIndexingOffset] = 1;
                m_Age_Temp[INDEX(1, ArrayIndexingOffset)] = 0;
            }

            ////////////////////////////////////////////////////////////////////////////////
            // Update mean, variance, and age
            //
            int nMatchIdx = m_ModelIdx[ArrayIndexingOffset];
            int obsWidthStep = m_Cur.cols;

            // obtain observation mean
            int nElemCnt[NUM_MODELS];
            float obs_mean[NUM_MODELS];
            float obs_var[NUM_MODELS];

            memset(nElemCnt, 0, sizeof(int) * NUM_MODELS);
            memset(obs_mean, 0, sizeof(float) * NUM_MODELS);
            memset(obs_var, 0, sizeof(float) * NUM_MODELS);

            for (int jj = 0; jj < BLOCK_SIZE; ++jj) {
                for (int ii = 0; ii < BLOCK_SIZE; ++ii) {

                    int idx_i = idx_base_i + ii;
                    int idx_j = idx_base_j + jj;

                    if (idx_i < 0 || idx_i >= inputImageCols || idx_j < 0 || idx_j >= inputImageRows)
                        continue;

                    obs_mean[nMatchIdx] += pCur[idx_i + idx_j * obsWidthStep];

                    float pixelDist = 0.0;
                    float fDiff = pCur[idx_i + idx_j * obsWidthStep] - m_Mean[INDEX(nMatchIdx, ArrayIndexingOffset)];
                    pixelDist += pow(fDiff, (int)2);

                    m_DistImg[idx_i + idx_j * obsWidthStep] = (float)pow(pCur[idx_i + idx_j * obsWidthStep] - m_Mean[INDEX(0, ArrayIndexingOffset)], 2);

                    if (m_Age_Temp[INDEX(0, ArrayIndexingOffset)] > 1) {
                        BYTE valOut = m_DistImg[idx_i + idx_j * obsWidthStep] > VAR_THRESH_FG_DETERMINE * m_Var_Temp[INDEX(0, ArrayIndexingOffset)] ? (BYTE)255 : (BYTE)0;
                        pOut[idx_i + idx_j * obsWidthStep] = valOut;
                    }

                    obs_var[nMatchIdx] = MAX(obs_var[nMatchIdx], pixelDist);

                    nElemCnt[nMatchIdx]++;
                }
            }

            for (int m = 0; m < NUM_MODELS; ++m) {

                if (nElemCnt[m] <= 0) {
                    m_Mean[INDEX(m, ArrayIndexingOffset)] = m_Mean_Temp[INDEX(m, ArrayIndexingOffset)];
                    m_Var[INDEX(m, ArrayIndexingOffset)] = m_Var_Temp[INDEX(m, ArrayIndexingOffset)];
                    m_Age[INDEX(m, ArrayIndexingOffset)] = m_Age_Temp[INDEX(m, ArrayIndexingOffset)];
                } else {
                    // learning rate for this block
                    float age = m_Age_Temp[INDEX(m, ArrayIndexingOffset)];
                    float alpha = age / (age + 1.0f);

                    obs_mean[m] /= ((float)nElemCnt[m]);

                    // Update with this mean and variance
                    if (age < 1) {
                        m_Mean[INDEX(m, ArrayIndexingOffset)] = obs_mean[m];
                        m_Var[INDEX(m, ArrayIndexingOffset)] = MAX(obs_var[m], INIT_BG_VAR);
                    } else {
                        m_Mean[INDEX(m, ArrayIndexingOffset)] = alpha * m_Mean_Temp[INDEX(m, ArrayIndexingOffset)] + (1.0f - alpha) * obs_mean[m];
                        m_Var[INDEX(m, ArrayIndexingOffset)] = alpha * m_Var_Temp[INDEX(m, ArrayIndexingOffset)] + (1.0f - alpha) * obs_var[m];
                        m_Var[INDEX(m, ArrayIndexingOffset)] = MAX(m_Var[INDEX(m, ArrayIndexingOffset)], MIN_BG_VAR);
                    }

                    // Update Age
                    m_Age[INDEX(m, ArrayIndexingOffset)] = m_Age_Temp[INDEX(m, ArrayIndexingOffset)] + 1.0f;
                    m_Age[INDEX(m, ArrayIndexingOffset)] = MIN(m_Age[INDEX(m, ArrayIndexingOffset)], MAX_BG_AGE);
                }
            }
        }
    }		// loop for models
}
#else
void ProbModel::UpdateSequential(cv::Mat &pOutputImg) {

    BYTE *pOut = pOutputImg.data;

    int curModelWidth = modelWidth;
    int curModelHeight = modelHeight;

    BYTE *pCur = m_Cur.data;
    int obsWidthStep = m_Cur.cols;

    //////////////////////////////////////////////////////////////////////////
    // Find Matching Model
    std::fill(m_ModelIdx.begin(), m_ModelIdx.end(), 0);

    //#pragma omp for
    for (int bIdx_j = 0; bIdx_j < curModelHeight; bIdx_j++) {
        for (int bIdx_i = 0; bIdx_i < curModelWidth; bIdx_i++) {
            const int idx_base_i = bIdx_i * BLOCK_SIZE;
            const int idx_base_j = bIdx_j * BLOCK_SIZE;
            const int ArrayIndexingOffset = bIdx_i + bIdx_j * modelWidth;

            float cur_mean = 0;
            float elem_cnt = 0;
            for (int jj = 0; jj < BLOCK_SIZE; ++jj) {
                for (int ii = 0; ii < BLOCK_SIZE; ++ii) {

                    int idx_i = idx_base_i + ii;
                    int idx_j = idx_base_j + jj;

                    if (idx_i < 0 || idx_i >= inputImageCols || idx_j < 0 || idx_j >= inputImageRows)
                        continue;

                    cur_mean += pCur[idx_i + idx_j * obsWidthStep];
                    elem_cnt += 1.0;
                }
            }    //loop for pixels
            cur_mean /= elem_cnt;

            //////////////////////////////////////////////////////////////////////////
            // Make Oldest Idx to 0 (swap)
            int oldIdx = 0;
            float oldAge = 0;
            for (int m = 0; m < NUM_MODELS; ++m) {
                float fAge = m_Age_Temp[INDEX(m, ArrayIndexingOffset)];

                if (fAge >= oldAge) {
                    oldIdx = m;
                    oldAge = fAge;
                }
            }
            if (oldIdx != 0) {
                m_Mean_Temp[INDEX(0, ArrayIndexingOffset)] = m_Mean_Temp[INDEX(oldIdx, ArrayIndexingOffset)];
                m_Mean_Temp[INDEX(oldIdx, ArrayIndexingOffset)] = cur_mean;

                m_Var_Temp[INDEX(0, ArrayIndexingOffset)] = m_Var_Temp[INDEX(oldIdx, ArrayIndexingOffset)];
                m_Var_Temp[INDEX(oldIdx, ArrayIndexingOffset)] = INIT_BG_VAR;

                m_Age_Temp[INDEX(0, ArrayIndexingOffset)] = m_Age_Temp[INDEX(oldIdx, ArrayIndexingOffset)];
                m_Age_Temp[INDEX(oldIdx, ArrayIndexingOffset)] = 0;
            }

            //////////////////////////////////////////////////////////////////////////
            // Select Model
            // Check Match against 0
            if (pow(cur_mean - m_Mean_Temp[INDEX(0, ArrayIndexingOffset)], (int) 2) <
                VAR_THRESH_MODEL_MATCH * m_Var_Temp[INDEX(0, ArrayIndexingOffset)]) {
                m_ModelIdx[ArrayIndexingOffset] = 0;
            }
                // Check Match against 1
            else if (pow(cur_mean - m_Mean_Temp[INDEX(1, ArrayIndexingOffset)], (int) 2) <
                     VAR_THRESH_MODEL_MATCH * m_Var_Temp[INDEX(1, ArrayIndexingOffset)]) {
                m_ModelIdx[ArrayIndexingOffset] = 1;
            }
                // If No match, set 1 age to zero and match = 1
            else {
                m_ModelIdx[ArrayIndexingOffset] = 1;
                m_Age_Temp[INDEX(1, ArrayIndexingOffset)] = 0;
            }

        }
    }        // loop for models

    // update with current observation
    float obs_mean[NUM_MODELS];
    float obs_var[NUM_MODELS];

    //#pragma omp for
    for (int bIdx_j = 0; bIdx_j < curModelHeight; bIdx_j++) {
        for (int bIdx_i = 0; bIdx_i < curModelWidth; bIdx_i++) {
            const int idx_base_i = bIdx_i * BLOCK_SIZE;
            const int idx_base_j = bIdx_j * BLOCK_SIZE;
            const int ArrayIndexingOffset = bIdx_i + bIdx_j * modelWidth;

            int nMatchIdx = m_ModelIdx[bIdx_i + bIdx_j * modelWidth];

            // obtain observation mean
            memset(obs_mean, 0, sizeof(float) * NUM_MODELS);
            int nElemCnt[NUM_MODELS];
            memset(nElemCnt, 0, sizeof(int) * NUM_MODELS);
            for (int jj = 0; jj < BLOCK_SIZE; ++jj) {
                for (int ii = 0; ii < BLOCK_SIZE; ++ii) {

                    int idx_i = idx_base_i + ii;
                    int idx_j = idx_base_j + jj;

                    if (idx_i < 0 || idx_i >= inputImageCols || idx_j < 0 || idx_j >= inputImageRows)
                        continue;

                    obs_mean[nMatchIdx] += pCur[idx_i + idx_j * obsWidthStep];
                    ++nElemCnt[nMatchIdx];
                }
            }
            for (int m = 0; m < NUM_MODELS; ++m) {

                if (nElemCnt[m] <= 0) {
                    m_Mean[INDEX(m, ArrayIndexingOffset)] = m_Mean_Temp[INDEX(m, ArrayIndexingOffset)];
                } else {
                    // learning rate for this block
                    float age = m_Age_Temp[INDEX(m, ArrayIndexingOffset)];
                    float alpha = age / (age + 1.0);

                    obs_mean[m] /= ((float) nElemCnt[m]);
                    // update with this mean
                    if (age < 1) {
                        m_Mean[INDEX(m, ArrayIndexingOffset)] = obs_mean[m];
                    } else {
                        m_Mean[INDEX(m, ArrayIndexingOffset)] =
                                alpha * m_Mean_Temp[INDEX(m, ArrayIndexingOffset)] +
                                (1.0f - alpha) * obs_mean[m];
                    }

                }
            }
        }
    }

    //#pragma omp for
    for (int bIdx_j = 0; bIdx_j < curModelHeight; bIdx_j++) {
        for (int bIdx_i = 0; bIdx_i < curModelWidth; bIdx_i++) {
            const int idx_base_i = bIdx_i * BLOCK_SIZE;
            const int idx_base_j = bIdx_j * BLOCK_SIZE;
            const int ArrayIndexingOffset = bIdx_i + bIdx_j * modelWidth;

            int nMatchIdx = m_ModelIdx[ArrayIndexingOffset];

            // obtain observation variance
            memset(obs_var, 0, sizeof(float) * NUM_MODELS);
            int nElemCnt[NUM_MODELS];
            memset(nElemCnt, 0, sizeof(int) * NUM_MODELS);
            for (int jj = 0; jj < BLOCK_SIZE; ++jj) {
                for (int ii = 0; ii < BLOCK_SIZE; ++ii) {

                    int idx_i = idx_base_i + ii;
                    int idx_j = idx_base_j + jj;
                    nElemCnt[nMatchIdx]++;

                    if (idx_i < 0 || idx_i >= inputImageCols || idx_j < 0 || idx_j >= inputImageRows) {
                        continue;
                    }

                    float pixelDist = 0.0;
                    float fDiff = pCur[idx_i + idx_j * obsWidthStep] -
                                  m_Mean[INDEX(nMatchIdx, ArrayIndexingOffset)];
                    pixelDist += pow(fDiff, (int) 2);

                    m_DistImg[idx_i + idx_j * obsWidthStep] = (float)pow(
                            pCur[idx_i + idx_j * obsWidthStep] -
                            m_Mean[INDEX(0, ArrayIndexingOffset)], (int) 2);

                    if (m_Age_Temp[INDEX(0, ArrayIndexingOffset)] > 1) {

                        BYTE valOut = m_DistImg[idx_i + idx_j * obsWidthStep] >
                                      VAR_THRESH_FG_DETERMINE *
                                      m_Var_Temp[INDEX(0, ArrayIndexingOffset)] ? (BYTE)255 : (BYTE)0;

                        pOut[idx_i + idx_j * obsWidthStep] = valOut;
                    }

                    obs_var[nMatchIdx] = MAX(obs_var[nMatchIdx], pixelDist);
                }
            }

            for (int m = 0; m < NUM_MODELS; ++m) {

                if (nElemCnt[m] > 0) {
                    float age = m_Age_Temp[INDEX(m, ArrayIndexingOffset)];
                    float alpha = age / (age + 1.0);

                    // update with this variance
                    if (age == 0) {
                        m_Var[INDEX(m, ArrayIndexingOffset)] = MAX(obs_var[m], INIT_BG_VAR);
                    } else {
                        float alpha_var = alpha;    //MIN(alpha, 1.0 - MIN_NEW_VAR_OBS_PORTION);
                        m_Var[INDEX(m, ArrayIndexingOffset)] =
                                alpha_var * m_Var_Temp[INDEX(m, ArrayIndexingOffset)] +
                                (1.0f - alpha_var) * obs_var[m];
                        m_Var[INDEX(m, ArrayIndexingOffset)] = MAX(
                                m_Var[INDEX(m, ArrayIndexingOffset)], MIN_BG_VAR);
                    }

                    // Update Age
                    m_Age[INDEX(m, ArrayIndexingOffset)] =
                            m_Age_Temp[INDEX(m, ArrayIndexingOffset)] + 1.0f;
                    m_Age[INDEX(m, ArrayIndexingOffset)] = MIN(
                            m_Age[INDEX(m, ArrayIndexingOffset)], MAX_BG_AGE);
                } else {
                    m_Var[INDEX(m, ArrayIndexingOffset)] = m_Var_Temp[INDEX(m, ArrayIndexingOffset)];
                    m_Age[INDEX(m, ArrayIndexingOffset)] = m_Age_Temp[INDEX(m, ArrayIndexingOffset)];
                }
            }
        }
    }
}
#endif
