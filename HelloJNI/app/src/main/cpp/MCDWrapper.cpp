#include <thread>
#include "MCDWrapper.hpp"
#include "params.hpp"

void MCDWrapper::Init(const cv::Mat &img)
{
	// Allocate.
	//
	imgGray = cv::Mat(img.rows, img.cols, IPL_DEPTH_8U, 1);
	detect_img = cv::Mat(img.rows, img.cols, IPL_DEPTH_8U, 1);

	cv::cvtColor(img, imgTemp, CV_RGB2GRAY);
	cv::medianBlur(imgTemp, imgGray, 5);

	m_LucasKanade.Init(imgGray);
    BGModel.Init(imgGray);
}

void MCDWrapper::Run(cv::Mat &img)
{
    auto copyInput = [this, img]() {
        BGModel.CopyInputToRS(img);
    };

    std::thread copyInputThread(copyInput);

	// Pre-process.
	//
	cv::cvtColor(img, imgTemp, CV_RGB2GRAY);
	cv::medianBlur(imgTemp, imgGray, 5);

	// Calculate backward homography.
	//
	m_LucasKanade.RunTrack(imgGray);
	double* homographyMatrix = m_LucasKanade.GetHomography();
    BGModel.MotionCompensate(homographyMatrix, imgGray);

    copyInputThread.join();

	// Update BG Model and Detect.
	//
    BGModel.Update(detect_img);

    BGModel.DisplayResult(img);
}

void MCDWrapper::InitProbModelCacheDir(const char *appCacheDir)
{
	BGModel.InitRenderScript(appCacheDir);
}