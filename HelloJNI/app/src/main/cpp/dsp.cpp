//
// Created by Petar on 03.05.2019..
//

#include "dsp.h"
#include <RenderScript.h>
#include <ScriptC_compute.h>
#include <android/log.h>

void Dsp::call_rs() {
    sp<RS> rs = new RS();
    rs->init(appCacheDir);

    sp<const Element> e = Element::U32(rs);
    sp<const Type> t = Type::create(rs, e, 100, 0, 0);
    uint32_t* input = new uint32_t[100];
    for (uint32_t i = 0; i < 100; i++) input[i] = i;

    sp<Allocation> inAlloc = Allocation::createTyped(rs, t);
    inAlloc->copy1DFrom(input);

    __android_log_print(ANDROID_LOG_INFO, "DebugTag", "First three values input are: %d, %d, %d", input[0], input[1], input[2]);
    uint32_t* output = new uint32_t[100];
    inAlloc->copy1DTo(output);

    sp<Allocation> outAlloc = Allocation::createTyped(rs, t);

    ScriptC_compute *script = new ScriptC_compute(rs);
    script->forEach_doubleTheInput(inAlloc, outAlloc);

    outAlloc->copy1DTo(output);
    __android_log_print(ANDROID_LOG_INFO, "DebugTag", "First three values output are, after doubling: %d, %d, %d", output[0], output[1], output[2]);
    script->forEach_tripleTheInput(outAlloc, inAlloc);
    rs->finish();

    inAlloc->copy1DTo(output);
    __android_log_print(ANDROID_LOG_INFO, "DebugTag", "First three values output are, after tripling: %d, %d, %d", output[0], output[1], output[2]);
}